package gr.ntua.softeng.affordances.proxy.repository.entities;

import javax.persistence.*;

import gr.ntua.softeng.affordances.proxy.repository.EntityValidator;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;

import java.time.LocalDate;
import java.time.Period;
import java.util.Set;

@Entity
@Table(name="USERS_DATA")
public class UserData implements EntityValidator<UserData> {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column	
	private String name;

	@Column
	@ElementCollection(fetch = FetchType.EAGER, targetClass=String.class)
	private Set<String> interests;

	@Column
	private LocalDate birthDate;

	public UserData(){
		
	}
	
	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public UserData validated() throws EntityValidationException {
		return this;
	}

	
	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	
    
    public String getName(){
    	return name;
    }
    
    public void setName(String name){
    	this.name = name;
    }

    @Transient
    public Integer getAge(){
    	if (birthDate == null){
			return null;
		}
    	Period period = Period.between(birthDate, LocalDate.now());
    	return period.getYears();
    }

	public Set<String> getInterests() {
		return interests;
	}

	public void setInterests(Set<String> interests) {
		this.interests = interests;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
}
