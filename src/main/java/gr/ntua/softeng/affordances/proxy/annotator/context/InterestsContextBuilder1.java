package gr.ntua.softeng.affordances.proxy.annotator.context;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.builder.DefaultContextBuilder;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.utils.SystemData;

import javax.annotation.Nonnull;

public class InterestsContextBuilder1 extends DefaultContextBuilder {

    private static String resourceNameUserData = UserData.class.getName();
    private static String resourceNameSystemData = SystemData.class.getName();

    @Override
    @Nonnull
    public AffordanceContext build() {

        ContextKey monthKey = new ContextKey(resourceNameSystemData, "month");
        ContextKey interestKey = new ContextKey(resourceNameUserData, "interest");
        ContextKey ageKey = new ContextKey(resourceNameUserData, "age");

        super.addConjunction(interestKey, ageKey).addConjunction(monthKey);
        return super.build();

    }
}
