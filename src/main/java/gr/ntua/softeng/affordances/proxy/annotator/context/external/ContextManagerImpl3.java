package gr.ntua.softeng.affordances.proxy.annotator.context.external;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;

import javax.annotation.Nullable;

public class ContextManagerImpl3 implements ContextManager {

	@Override
	public void populate(AffordanceContext context, @Nullable Object object)
			throws ClassCastException, UnsupportedOperationException {
		throw new  UnsupportedOperationException("ContextManagerImpl3 not supported");
	}

	@Override
	public Class<?> getResourceClass() {
		throw new  UnsupportedOperationException("ContextManagerImpl3 not supported");
	}

	@Override
	public List<String> getResourceFields() {
		throw new  UnsupportedOperationException("ContextManagerImpl3 not supported");
	}

	@Override
	public Collection<String> getResourceFieldOperations(String field) {
		throw new  UnsupportedOperationException("ContextManagerImpl3 not supported");
	}

	@Override
	public int compare(ContextKey contextKey, ContextValue val1, String val2) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("ContextManagerImpl3 not supported");
	}

}