package gr.ntua.softeng.affordances.proxy.evaluator.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.escalon.hypermedia.affordance.Affordance;
import gr.ntua.softeng.gm.DecompositionType;
import gr.ntua.softeng.gm.SimpleGoalModelNode;
import gr.ntua.softeng.gm.reasoning.wfr.LowHighNodeValue;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;

public class ModelB extends ExtendedModel<SimpleGoalModelNode> {
	
	private static final ContextKey contextKey = new ContextKey(
			"UserData", "name");
	
	public ModelB(String name){
		super(name);
		
		this.contextKeys.add(contextKey);

		Affordance affordance = new Affordance("http://www.google.com/KostasORMike", "affordance");
		this.setAffordance(affordance);

		SimpleGoalModelNode rootNode = new SimpleGoalModelNode("root");
		List<SimpleGoalModelNode> childNodes = new ArrayList<>();
		childNodes.add(new SimpleGoalModelNode("name1"));
		childNodes.add(new SimpleGoalModelNode("name2"));
		this.addDecomposition(rootNode, childNodes, DecompositionType.OR);
	}
	
	@Override
	public Map<String, LowHighNodeValue> getInitialValues(AffordanceContext context){
		
		double val1 = 0, val2 = 0;

		ContextValue contextValue = context.getValue(contextKey);

		if (contextValue != null &&
				"Kostas".equals(contextValue.getValue())){
			val1 = 50;
		}

		if (contextValue != null &&
				"Mike".equals(contextValue.getValue())){
			val2 = 50;
		}

		// Initial Values - Fuzzification
		Map<String, LowHighNodeValue> initialValues = new HashMap<>();
		initialValues.put("name1", valueHelper.getValue(val1));
		initialValues.put("name2",  valueHelper.getValue(val2));
		
		return initialValues;
	}
}
