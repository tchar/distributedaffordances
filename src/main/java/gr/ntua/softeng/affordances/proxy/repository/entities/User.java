package gr.ntua.softeng.affordances.proxy.repository.entities;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.*;

import gr.ntua.softeng.affordances.proxy.repository.EntityValidator;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;

@Entity
@Table(name="USERS")
public class User implements EntityValidator<User> {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String username;

	@OneToOne(optional = false, orphanRemoval=true)
	private UserData userData;
    
	
	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private UserRole userRole;
	

    public User(){
    	
    }

	public User(String username, String password, UserRole userRole) {
		this.username = username;
		this.password = password;
		this.userRole = userRole;
	}


	private void validate() throws EntityValidationException {
    	if (username == null)
    		throw new EntityValidationException("username must not be null");
		if (password == null){
			throw new EntityValidationException("password must not be null");
		}
		if (userRole == null){
			throw new EntityValidationException("user role must not be null");
		}
		
    	String p1 = "^[a-zA-Z0-9_]+$";
    	
		if (!Pattern.compile(p1).matcher(username).find()){
			throw new EntityValidationException("Bad username \"" + username + "\". Example: John123_4");
		}

		String p2 = "^[^\\s\\\\]+$";
		if (!Pattern.compile(p2).matcher(password).find()){
			throw new EntityValidationException("Bad password \"" + password + "\".Example: k1!LOaksd");
		}
    }

	@Override
	public boolean isValid() {
		try {
			validate();
		} catch (EntityValidationException e){
			return false;
		}
		return true;
	}
    
    @Override
    public User validated() throws EntityValidationException {
    	validate();
		return this;
    }
    
	public Long getId(){
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
		
    
    public String getUsername(){
    	return username;
    }
    
    public void setUsername(String username){
    	this.username = username;
    }

    public String getPassword(){
    	return password;
    }
    
    public void setPassword(String password){
    	this.password = password;
    }

    public void setUserData(UserData userData){
    	this.userData = userData;
	}

	public UserData getUserData(){
    	return this.userData;
	}
    
    @Override
    public boolean equals(Object obj){
    	if (obj == null) return false;
    	if (!(obj instanceof User)) return false;
    	
    	User user = (User) obj;
		return Objects.equals(username, user.username) &&
				Objects.equals(password, user.password);
    }

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
}
