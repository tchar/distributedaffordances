package gr.ntua.softeng.affordances.proxy.annotator.context;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.builder.DefaultContextBuilder;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.utils.SystemData;

import javax.annotation.Nonnull;

public class ContextBuilderImpl2 extends DefaultContextBuilder {

    private static String resourceNameUserData = UserData.class.getName();
    private static String resourceNameSystemData = SystemData.class.getName();

    public ContextBuilderImpl2(){
    }


    @Override
    @Nonnull
    public AffordanceContext build() {

        ContextKey ageKey = new ContextKey(resourceNameUserData, "age");
        ContextKey timeKey = new ContextKey(resourceNameSystemData, "time");
        ContextKey nameKey = new ContextKey(resourceNameUserData, "name");
        ContextKey  seasonKey = new ContextKey(resourceNameSystemData, "season");

        super.addConjunction(ageKey, seasonKey).addConjunction(nameKey, timeKey);
        return super.build();

    }
}
