package gr.ntua.softeng.affordances.proxy.repository.entities;

public enum UserRole {
	ADMIN, USER;
}
