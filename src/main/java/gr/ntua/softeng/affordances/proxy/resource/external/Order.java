package gr.ntua.softeng.affordances.proxy.resource.external;

public class Order{

	private String name;
	private double cost;
	
	public Order(){
		
	}
	
	public Order(String id, String name, int cost) {
		this.setName(name);
		this.setCost(cost);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}
	
}
