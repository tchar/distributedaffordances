package gr.ntua.softeng.affordances.proxy.repository.goalmodel;

import gr.ntua.softeng.affordances.proxy.repository.ExtendedRepository;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import org.springframework.beans.factory.annotation.Autowired;

import gr.ntua.softeng.affordances.proxy.repository.entities.GoalModel;

public class GoalModelRepositoryImpl implements ExtendedRepository<GoalModel> {
	
	@Autowired
	private GoalModelRepository goalModelRepository;
	
	@Override
	public GoalModel saveOrUpdate(GoalModel goalModel) throws EntityValidationException {
		goalModel = goalModel.validated();
		GoalModel foundModel = goalModelRepository.findByName(goalModel.getName());
		if (foundModel != null){
			goalModel.setId(foundModel.getId());
		}
		return goalModelRepository.save(goalModel);
	}
}
