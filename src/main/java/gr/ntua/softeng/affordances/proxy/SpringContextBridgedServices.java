package gr.ntua.softeng.affordances.proxy;

import gr.ntua.softeng.affordances.framework.evaluator.Evaluator;
import gr.ntua.softeng.affordances.proxy.annotator.context.ContextFactory;
import gr.ntua.softeng.affordances.proxy.evaluator.models.ExtendedModelFactory;
import gr.ntua.softeng.affordances.proxy.resource.ResourceFactory;
import gr.ntua.softeng.affordances.proxy.utils.SystemUtils;

public interface SpringContextBridgedServices {
    SystemUtils getSystemUtils();
    ContextFactory contextFactory();
    ExtendedModelFactory modelFactory();
    Evaluator evaluator();
    ResourceFactory resourceFactory();
}
