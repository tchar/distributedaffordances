package gr.ntua.softeng.affordances.proxy.security;

import gr.ntua.softeng.affordances.proxy.repository.entities.User;
import gr.ntua.softeng.affordances.proxy.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;

import gr.ntua.softeng.affordances.proxy.repository.entities.UserRole;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BackendAdminUsernamePasswordAuthenticationProvider implements AuthenticationProvider {

    public static final String INVALID_BACKEND_ADMIN_CREDENTIALS = "Invalid Backend Admin Credentials";
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String username, password;
        try{
            username = (String) authentication.getPrincipal();
            password = (String) authentication.getCredentials();
        } catch (ClassCastException e){
            throw new BadCredentialsException(e.getMessage());
        }

        if (credentialsMissing(username, password) || credentialsInvalid(username, password)) {
            throw new BadCredentialsException(INVALID_BACKEND_ADMIN_CREDENTIALS);
        }

        return new UsernamePasswordAuthenticationToken(username, null,
                AuthorityUtils.commaSeparatedStringToAuthorityList(UserRole.ADMIN.toString()));
    }

    private boolean credentialsMissing(String username, String password) {
        return username == null || password == null;
    }

    private boolean credentialsInvalid(String username, String password) {
    	User user = userRepository.findUserByUsername(username);
    	if (user == null || !encoder.matches(password, user.getPassword()))
    		return true;

    	return user.getUserRole() != UserRole.ADMIN;
    }
    
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(BackendAdminUsernamePasswordAuthenticationToken.class);
    }
}