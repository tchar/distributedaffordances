package gr.ntua.softeng.affordances.proxy.repository;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component("databaseMigrator")
public class AffordancesDatabaseMigrator implements DatabaseMigrator {

	private static AffordancesDatabaseMigrator instance;

	private AffordancesDatabaseMigrator(){
		
	}

	public static AffordancesDatabaseMigrator getInstance() {
		if (instance == null){
			instance = new AffordancesDatabaseMigrator();
		}
		return instance;
	}

	
	@Override
	public void migrate(Object obj, Class<?> objClass) {
	}

	@Override
	public void migrate(HttpServletResponse response, Object obj, Class<?> objClass) {
	}

	@Override
	public void migrate(HttpServletResponse response, HttpServletRequest request, Object obj, Class<?> objClass) {
	}
}
