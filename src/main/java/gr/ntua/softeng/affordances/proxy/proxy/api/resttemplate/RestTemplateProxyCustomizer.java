package gr.ntua.softeng.affordances.proxy.proxy.api.resttemplate;

import java.net.MalformedURLException;
import java.net.URL;

import gr.ntua.softeng.affordances.framework.mapper.ResourceMapper;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class RestTemplateProxyCustomizer implements RestTemplateCustomizer{

	private ResourceMapper resourceMapper;
	
	public RestTemplateProxyCustomizer(ResourceMapper resourceMapper){
		this.resourceMapper = resourceMapper;
	}
	

	@Override
    public void customize(RestTemplate restTemplate) {
		HttpComponentsClientHttpRequestFactory factory = test();
		if (factory != null){
			restTemplate.setRequestFactory(factory);
		}
    }

	private HttpComponentsClientHttpRequestFactory test(){
		URL url;
		try {
			url = new URL(resourceMapper.getTranslatedBaseAddress());
		} catch (MalformedURLException e) {
			return null;
		}

		int port = url.getPort();
		if (port == -1)
			port = url.getDefaultPort();
		
		HttpHost myProxy = new HttpHost(url.getHost(), port);
        HttpClientBuilder clientBuilder = HttpClientBuilder.create();
        clientBuilder.setProxy(myProxy);
        HttpClient httpClient = clientBuilder.build();
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setHttpClient(httpClient);
		return factory;
	}
}
