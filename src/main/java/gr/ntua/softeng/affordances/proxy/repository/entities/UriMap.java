package gr.ntua.softeng.affordances.proxy.repository.entities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.persistence.*;

import gr.ntua.softeng.affordances.proxy.repository.EntityValidator;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.affordances.proxy.resource.ResourceFactory;
import org.springframework.util.AntPathMatcher;

@Entity
@Table(
		name="URI_MAPS",
		uniqueConstraints={@UniqueConstraint(columnNames={"SERVICE_NAME", "RESOURCE_NAME", "HTTP_METHOD"})}
)
public class UriMap implements EntityValidator<UriMap>{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, name="SERVICE_NAME")
	private String serviceName;

	@Column(nullable = false, name="RESOURCE_NAME")
	private String resourceName;

	@Column(nullable = false, name="HTTP_METHOD")
	private HttpMethod httpMethod;

	@Column(nullable = false)
	private String forwardBaseAddress;

	@Transient
	private String originalPath;

	@Column(nullable = false)
	private String forwardPath;
	
	@Column(nullable = false)
	private String resourceClassName;

	@Nullable
	@Column
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private List<HttpHeader> httpHeaders;
	

	public UriMap(){
		
	}
	
	public UriMap(String originalPath, String forwardBaseAddress,
				  String forwardPath, String resourceClassName, HttpMethod httpMethod){

		this.originalPath = originalPath;
		this.forwardBaseAddress = forwardBaseAddress;
		this.forwardPath = forwardPath;
		this.resourceClassName = resourceClassName;
		this.httpMethod = httpMethod;
	}
	
	private void validate() throws EntityValidationException {
		if (originalPath == null){
			throw new EntityValidationException("Original Path must not be null");
		}

		if (httpMethod == null){
			throw new EntityValidationException("Http Method must not be null");
		}

		if (forwardBaseAddress == null){
			throw new EntityValidationException("Forward Base Address must not be null");
		}

		if (forwardPath == null){
			throw new EntityValidationException("Forward Path must not be null");
		}

		if (resourceClassName == null){
			throw new EntityValidationException("Resource Class Name must not be null");
		}
		
//		String s1 = "^(http:\\/\\/)*(\\w+\\.{0,1})+(:\\d+)*$";
//		Matcher m = Pattern.compile(s1).matcher(forwardBaseAddress);
		
		// TODO refine this
		try{
			URL url = new URL(forwardBaseAddress);
			if (!url.getProtocol().equals("http"))
				throw new EntityValidationException("Only http protocol is currently supported");
		} catch (MalformedURLException e){
			throw new EntityValidationException("Bad Forward Base Address. Example: http://localhost:8090");
		}

		AntPathMatcher matcher = new AntPathMatcher();

		String basePattern = "/{service:[a-zA-Z0-9-_\\.]+}/{resource:[a-zA-Z0-9-_\\.]+}";

		if (!matcher.match(basePattern, originalPath)){
			throw new EntityValidationException("Bad Original Path. Example: /ebay/order");
		}

		Map<String, String> map = matcher.extractUriTemplateVariables(basePattern, originalPath);
		if (map.get("service") == null || map.get("resource") == null){
			throw new EntityValidationException("Bad Original Path. Format must be /{service}/{resource}");
		}

		serviceName = map.get("service");
		resourceName = map.get("resource");

		String s2 = "^(/[a-zA-Z0-9_.-]+)+$";
		Pattern p = Pattern.compile(s2);

		if (!p.matcher(forwardPath).find()){
			throw new EntityValidationException("Bad Forward Path. Example: /other-api/order");
		}
		
		if (!ResourceFactory.getInstance().getExternalResourceClassNames().contains(resourceClassName)){
			throw new EntityValidationException("Resource \"" + resourceClassName + "\" does not exist.");
		}

		if (httpHeaders != null){
			for (HttpHeader httpHeader : httpHeaders){
				httpHeader.validated();
			}
		}
	}
	
	@Override
	public boolean isValid() {
		try {
			validate();
		} catch (EntityValidationException e){
			return false;
		}
		return true;
	}
	
	@Override
	public UriMap validated() throws EntityValidationException {
		validate();
		return this;
	}
	
	public String getForwardBaseAddress() {
		return forwardBaseAddress;
	}
	
	public Long getId(){
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
			
	public void setForwardBaseAddress(String forwardBaseAddress) {
		this.forwardBaseAddress = forwardBaseAddress;
	}

	@Transient
	public String getOriginalPath(){
		return "/" + this.serviceName + "/" + this.resourceName;
	}

	@Transient
	public void setOriginalPath(String originalPath) {
		this.originalPath = originalPath;
	}
	
	public String getForwardPath(){
		return forwardPath;
	}
	
	public void setForwardPath(String forwardPath){
		this.forwardPath = forwardPath;
	}
	
	public String getResourceClassName(){
		return resourceClassName;
	}
	
	public void setResourceClassName(String resourceClass){
		this.resourceClassName = resourceClass;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public List<HttpHeader> getHttpHeaders() {
		return httpHeaders;
	}

	public void setHttpHeaders(List<HttpHeader> httpHeaders) {
		this.httpHeaders = httpHeaders;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public enum HttpMethod{
		GET, POST, DELETE;

		public static HttpMethod parse(String method) {
			switch (method.toUpperCase()){
				case "GET":
					return GET;
				case "POST":
					return POST;
				case "DELETE":
					return DELETE;
				default:
					return GET;
			}
		}
	}
}
