package gr.ntua.softeng.affordances.proxy.security;

import gr.ntua.softeng.affordances.proxy.repository.entities.User;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserRole;
import gr.ntua.softeng.affordances.proxy.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class DomainUsernamePasswordAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String username, password;
        try{
            username = (String) authentication.getPrincipal();
            password = (String) authentication.getCredentials();
        } catch (ClassCastException e){
            throw new BadCredentialsException(e.getMessage());
        }

        if (username == null || password == null) {
            throw new BadCredentialsException("User Credentials not present");
        }

        User user = userRepository.findUserByUsername(username);
        if (user == null || !encoder.matches(password, user.getPassword())){
            throw new BadCredentialsException("Invalid User Credentials");
        }

        if (user.getUserRole() != UserRole.USER){
            throw new BadCredentialsException("Invalid User Credentials");
        }

        return new UsernamePasswordAuthenticationToken(user, null,
                AuthorityUtils.commaSeparatedStringToAuthorityList(UserRole.USER.toString()));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}