package gr.ntua.softeng.affordances.proxy.repository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface DatabaseMigrator {
	
	void migrate(Object obj, Class<?> objClass);
	void migrate(HttpServletResponse response, Object obj, Class<?> objClass);
	void migrate(HttpServletResponse response, HttpServletRequest request, Object obj, Class<?> objClass);

}
