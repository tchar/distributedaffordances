package gr.ntua.softeng.affordances.proxy.repository.userdata;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;

@RepositoryRestResource(exported = false)
public interface UserDataRepository extends CrudRepository<UserData, Long>{

}
