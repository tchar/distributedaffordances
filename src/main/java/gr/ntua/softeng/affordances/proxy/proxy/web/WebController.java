package gr.ntua.softeng.affordances.proxy.proxy.web;

import gr.ntua.softeng.affordances.proxy.ServerConfig;
import gr.ntua.softeng.affordances.proxy.evaluator.models.ExtendedModelFactory;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.affordances.proxy.repository.entities.*;
import gr.ntua.softeng.affordances.proxy.repository.urimap.UriMapRepository;
import gr.ntua.softeng.affordances.proxy.repository.user.UserRepository;
import gr.ntua.softeng.affordances.proxy.resource.JsonResource;
import gr.ntua.softeng.affordances.proxy.resource.NewUserResource;
import gr.ntua.softeng.affordances.proxy.resource.ResourceFactory;
import gr.ntua.softeng.affordances.proxy.annotator.context.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;


@Controller
@RequestMapping
public class WebController {

	private final static Logger LOGGER = LoggerFactory.getLogger(WebController.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UriMapRepository uriMapRepository;
	
	@Autowired
	private ExtendedModelFactory extendedModelFactory;
	
	@Autowired
	private ResourceFactory resourceFactory;
	
	@Autowired
	private ContextFactory contextFactory;
	
	@RequestMapping(ServerConfig.baseAdminPath)
    public String index(Model model) {
		model.addAttribute("mapper_editor_name", "Mapper Editor");
		model.addAttribute("goal_model_editor_name", "Goal Model Editor");
        return "index";
    }

	@RequestMapping(value = ServerConfig.baseAdminPath + "/mapper", method=RequestMethod.GET)
    public String mapperEditor(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
		model.addAttribute("mapper_editor_name", "Mapper Editor");
        return "mapper_editor";
    }
	
	@RequestMapping(ServerConfig.baseAdminPath + "/goalmodel")
    public String goalModelEditor(@RequestParam(value="name", required=false, defaultValue="World2") String name, Model model) {
		model.addAttribute("goal_model_editor_name", "Goal Model Editor");
        return "goal_model_editor";
    }

	@RequestMapping(value = "user", method = RequestMethod.GET)
	public String user(){
		return "new_user";
	}
	
	@RequestMapping(value = ServerConfig.baseAdminPath + "/mapper/getAll", method=RequestMethod.GET)
	public ResponseEntity<JsonResource> getMaps(){
		
		JsonResource json = new JsonResource();
		JsonResource.UriMapData data = new JsonResource.UriMapData();
		data.addMaps(uriMapRepository.findAll());
		data.setResources(resourceFactory.getExternalResourceClassNames());
		json.setData(data);
		
		return ResponseEntity.ok(json);
	}
	
	// TODO change this to PathVariable
	@RequestMapping(value = ServerConfig.baseAdminPath + "/mapper", method=RequestMethod.DELETE,
					consumes = "application/json", produces="application/json")
	public ResponseEntity<JsonResource> deleteMap(@RequestBody UriMap uriMap){
		
		JsonResource json = new JsonResource();
		JsonResource.UriMapData data = new JsonResource.UriMapData();
		
		try {
			uriMap.validated();
			String serviceName = uriMap.getServiceName();
			String resourceName = uriMap.getResourceName();
			UriMap.HttpMethod method = uriMap.getHttpMethod();
			uriMap = uriMapRepository
					.findUriMapByServiceNameAndResourceNameAndHttpMethod(serviceName, resourceName, method);
			uriMapRepository.delete(uriMap);
			LOGGER.debug("Map deleted");
			json.setMessage("Map deleted.");
			data.addMaps(uriMapRepository.findAll());
			data.setResources(resourceFactory.getExternalResourceClassNames());
			json.setData(data);
		} catch (DataAccessException e){
			LOGGER.error(e.getMessage());
			json.setMessage(e.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json);
		} catch (EntityValidationException e){
			LOGGER.error(e.getMessage());
			json.setMessage(e.getSpecificMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json);
		}
		
		return ResponseEntity.ok(json);
	}


	@RequestMapping(value = ServerConfig.baseAdminPath + "/mapper", method=RequestMethod.POST,
					consumes="application/json", produces="application/json")
	public ResponseEntity<JsonResource> newMap(@RequestBody UriMap uriMap){
		
		JsonResource json = new JsonResource();
		JsonResource.UriMapData data = new JsonResource.UriMapData();
		
		try{		
			uriMapRepository.saveOrUpdate(uriMap);
			LOGGER.debug("Saved new map into repository.");
			for (UriMap uriMap1 : uriMapRepository.findAll()) {
				data.addMap(uriMap1);
			}
			data.setResources(resourceFactory.getExternalResourceClassNames());
			json.setMessage("Map created.");
			json.setData(data);
		} catch (DataAccessException e){
			LOGGER.error(e.getMessage());
			json.setMessage(e.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json);
		} catch (EntityValidationException e){
			LOGGER.error(e.getMessage());
			json.setMessage(e.getSpecificMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json);
		}
		

		
		return ResponseEntity.ok(json);
	}
	
	@RequestMapping(value = ServerConfig.baseAdminPath + "/goalmodel/getAll", method=RequestMethod.GET,
					produces="application/json")
	public ResponseEntity<JsonResource> getModels(){
		
		JsonResource json = new JsonResource();
		JsonResource.GoalModelData data = new JsonResource.GoalModelData();
		
		data.setGoalModels(extendedModelFactory.getAllModels());
		data.setContextMap(contextFactory.getContextMap());
		data.setExternalContextKeys(contextFactory.getExternalContextKeys());
		json.setData(data);
		
		return ResponseEntity.ok(json);
	}
	
	@RequestMapping(value = ServerConfig.baseAdminPath + "/goalmodel", method=RequestMethod.POST,
					consumes="application/json", produces="application/json")
	public ResponseEntity<JsonResource> newModel(@RequestBody GoalModel gm){
		
		JsonResource json = new JsonResource();
		JsonResource.GoalModelData data = new JsonResource.GoalModelData();
		
		try{
			extendedModelFactory.addModel(gm);
			data.setGoalModels(extendedModelFactory.getAllModels());
			data.setContextMap(contextFactory.getContextMap());
			data.setExternalContextKeys(contextFactory.getExternalContextKeys());
			json.setMessage("Goal model updated.");
			json.setData(data);
		} catch (DataAccessException e){
			json.setMessage(e.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json);
		} catch (EntityValidationException e){
			LOGGER.error(e.getMessage());
			json.setMessage(e.getSpecificMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json);
		}

		return ResponseEntity.ok(json);
	}	
	
	@RequestMapping(value = ServerConfig.baseAdminPath + "/goalmodel/{modelName}", method=RequestMethod.DELETE,
					produces="application/json")
	public ResponseEntity<JsonResource> deleteModel(@PathVariable String modelName){
		
		JsonResource json = new JsonResource();
		JsonResource.GoalModelData data = new JsonResource.GoalModelData();
		HttpStatus httpStatus;


		try{
			extendedModelFactory.deleteModel(modelName);
			json.setMessage("Goal model deleted.");
			httpStatus = HttpStatus.OK;
		} catch (DataAccessException e){
			httpStatus = HttpStatus.BAD_REQUEST;
			json.setMessage("Could not delete goal model. Are you sure it exists?");
		}

		try{
			data.setGoalModels(extendedModelFactory.getAllModels());
			data.setContextMap(contextFactory.getContextMap());
			data.setExternalContextKeys(contextFactory.getExternalContextKeys());
			json.setData(data);
		} catch (DataAccessException ignored) {
			json.setMessage("Could not load goal models");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(json);
		}
		
		return ResponseEntity.status(httpStatus).body(json);
	}


	@RequestMapping(value = "user", method = RequestMethod.POST)
	public ResponseEntity<JsonResource> newUser(@RequestBody NewUserResource newUser){

		User user = new User();
		user.setUsername(newUser.getUsername());
		user.setPassword(newUser.getPassword());


		UserData userData = new UserData();
		userData.setName(newUser.getName());
		LocalDate birthDate = Instant
				.ofEpochMilli(newUser.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();
		userData.setBirthDate(birthDate);
		userData.setInterests(newUser.getInterests());

		user.setUserData(userData);

		user.setUserRole(UserRole.USER);

		JsonResource json = new JsonResource();
		HttpStatus httpStatus;

		try {
			if (userRepository.findUserByUsername(user.getUsername()) == null){
				userRepository.saveOrUpdate(user);
				httpStatus = HttpStatus.CREATED;
				json.setMessage("User created");
				LOGGER.debug("User created");
			} else {
				httpStatus = HttpStatus.CONFLICT;
				json.setMessage("User already exists");
				LOGGER.warn("User already exists");
			}
		} catch (EntityValidationException e) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			json.setMessage("Could not save user");
			LOGGER.error(e.getMessage());
		}

		return ResponseEntity.status(httpStatus).body(json);
	}
}
