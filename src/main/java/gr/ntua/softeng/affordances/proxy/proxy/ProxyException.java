package gr.ntua.softeng.affordances.proxy.proxy;

import gr.ntua.softeng.affordances.proxy.proxy.api.mapper.ExtendedResourceMapper;
import org.springframework.web.servlet.HandlerMapping;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

public class ProxyException extends RuntimeException {

    private final String specificErrorMessage;
    private final HttpServletRequest request;
    private final ExtendedResourceMapper mapper;

    public ProxyException(String message, HttpServletRequest request, @Nullable ExtendedResourceMapper mapper) {
        super(message);
        this.specificErrorMessage = message;
        this.request = request;
        this.mapper = mapper;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    @Nullable
    public ExtendedResourceMapper getMapper() {
        return mapper;
    }

    public String getSpecificErrorMessage() {
        return specificErrorMessage;
    }

    public String getPath(){
        return (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
    }
}
