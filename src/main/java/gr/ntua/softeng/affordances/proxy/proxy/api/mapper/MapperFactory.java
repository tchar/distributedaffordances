package gr.ntua.softeng.affordances.proxy.proxy.api.mapper;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import gr.ntua.softeng.affordances.framework.mapper.MapperException;
import gr.ntua.softeng.affordances.proxy.repository.entities.UriMap;
import gr.ntua.softeng.affordances.proxy.repository.urimap.UriMapRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

@Component("mapperFactory")
public class MapperFactory {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(MapperFactory.class);

	private static MapperFactory instance;
	
	@Autowired
	private UriMapRepository uriMapRepository;
	
	public ExtendedResourceMapper getResourceMapper(HttpServletRequest request, String serviceName
												, String resourceName) throws MapperException {
		UriMap.HttpMethod method = UriMap.HttpMethod.parse(request.getMethod());
		UriMap uriMap = uriMapRepository
				.findUriMapByServiceNameAndResourceNameAndHttpMethod(serviceName, resourceName, method);
		if (uriMap == null){
			throw new MapperException("Uri Map not found");
		}
		return new UriMapMapper(uriMap, request);
	}

	public List<UriMap> getAllMaps(){
		List<UriMap> ret = new LinkedList<>();
		for (UriMap uriMap : uriMapRepository.findAll()) {
			ret.add(uriMap);
		}
		return ret;
	}
}
