package gr.ntua.softeng.affordances.proxy.resource;

import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;

public class ApiResource{
	
	private String name;

	private Integer age;

	private Object payload;

	public ApiResource(){
		
	}
	
	public ApiResource(UserData userData, Object object){
		this.name = userData.getName();
		this.age = userData.getAge();
		this.payload = object;
	}
	
	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

    public void setUserData(UserData userData) {
        this.name = userData.getName();
        this.age = userData.getAge();
    }
}
