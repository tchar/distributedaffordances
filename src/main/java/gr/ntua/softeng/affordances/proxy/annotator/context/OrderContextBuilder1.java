package gr.ntua.softeng.affordances.proxy.annotator.context;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.builder.DefaultContextBuilder;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.resource.external.Order;

import javax.annotation.Nonnull;

public class OrderContextBuilder1 extends DefaultContextBuilder{

    private static String resourceNameUserData = UserData.class.getName();
    private static String resourceNameOrder = Order.class.getName();

    @Override
    @Nonnull
    public AffordanceContext build() {

        ContextKey orderCostKey = new ContextKey(resourceNameOrder, "cost");
        ContextKey ageKey = new ContextKey(resourceNameUserData, "age");

        super.addConjunction(orderCostKey).addConjunction(ageKey);
        return super.build();

    }
}
