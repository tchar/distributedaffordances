package gr.ntua.softeng.affordances.proxy.annotator.context;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.builder.DefaultContextBuilder;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;
import gr.ntua.softeng.affordances.proxy.SpringContextBridge;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.utils.SystemData;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * This class is an extension to the DefaultContextBuilder.
 * The main purpose of this class is to emulate the CNF formula
 * (A v B v C v F v G ...) in which all terms from A to G are
 * ContextKeys that refer to UserData, SystemData, SomeResource
 * resources. Where SomeResource is any resource that is represented
 * by a ContextManager passed to the constructor of this class.
 *
 * @author Tilemachos Charalampous
 */
public class ResourceDefaultContextBuilder extends DefaultContextBuilder {

    private final List<ContextManager> managers;

    public ResourceDefaultContextBuilder(List<ContextManager> managers){
        this.managers = managers;
    }

    private Collection<ContextKey> getContextKeys(ContextManager manager){
        List<ContextKey> keys = new LinkedList<>();
        Collection<String> fields = manager.getResourceFields();

        for (String field : fields){
            keys.add(new ContextKey(manager.getResourceClass().getName(), field));
        }
        return keys;
    }

    @Override
    @Nonnull
    public AffordanceContext build(){
        List<ContextKey> keys = new LinkedList<>();
        for (ContextManager manager: managers){
            keys.addAll(getContextKeys(manager));
        }

        super.setManagers(managers);

        return super.addConjunction(keys).build();
    }
}
