package gr.ntua.softeng.affordances.proxy.resource;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.utils.SystemData;
import org.glassfish.hk2.utilities.reflection.ParameterizedTypeImpl;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;


public class ResourceFactory {

	private static ResourceFactory instance;
	private static final String DEFAULT_PATH = ResourceFactory.class.getPackage().getName() + ".external";
	
	private static final String[] OTHER_RESOURCES = new String[]{UserData.class.getName(), SystemData.class.getName()};
	
	private Set<String> classNames;
	private Set<String> externalClassNames;
	
	private ResourceFactory(){
		classNames = new HashSet<>();
		externalClassNames = new HashSet<>();
		initExternal();
		initInternal();
	}
	
	private void initExternal(){
		List<ClassLoader> classLoadersList = new LinkedList<>();
		classLoadersList.add(ClasspathHelper.contextClassLoader());
		classLoadersList.add(ClasspathHelper.staticClassLoader());
	
		Reflections reflections = new Reflections(new ConfigurationBuilder()
		    .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
		    .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
		    .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(DEFAULT_PATH))));


		for (Class<?> aClass : reflections.getSubTypesOf(Object.class)) {
			String s = aClass.getName();
			externalClassNames.add(s);
			classNames.add(s);
		}
	}
	
	private void initInternal(){
		classNames.addAll(Arrays.asList(OTHER_RESOURCES));
	}
	
	public static ResourceFactory getInstance(){
		if (instance == null){
			instance = new ResourceFactory();
		}
		return instance;
	}
	
	public Class<?> getResourceClass(String c) throws ClassNotFoundException{
		return Class.forName(c);
	}
	
	public Set<String> getResourceClassNames(){
		return this.classNames;
	}
	
	public Set<String> getExternalResourceClassNames(){
		return this.externalClassNames;
	}
	
	public ParameterizedTypeReferenceWrapper<Resource<Object>> 
					getResourceParameterizedTypeReferece(String className) throws ClassNotFoundException{
		Class<?> classType = Class.forName(className);
		return getResourceParameterizedTypeReferece(classType);
	}
	
	public ParameterizedTypeReferenceWrapper<Resource<Object>> 
								getResourceParameterizedTypeReferece(Class<?> classType) {
		return new ParameterizedTypeReferenceWrapper<>(classType);
	}
	
	private class ParameterizedTypeReferenceWrapper<T> extends ParameterizedTypeReference<T> {
	
		private Class<?> classType;
		
		public ParameterizedTypeReferenceWrapper(Class<?> classType){
			this.classType = classType;
		}
		
		@Override
		public Type getType() {
			Type [] responseWrapperActualTypes = {classType};
			return new ParameterizedTypeImpl(Resource.class, responseWrapperActualTypes);
		}
	}
}
