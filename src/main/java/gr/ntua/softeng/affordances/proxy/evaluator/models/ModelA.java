package gr.ntua.softeng.affordances.proxy.evaluator.models;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.*;

import de.escalon.hypermedia.affordance.Affordance;
import gr.ntua.softeng.gm.DecompositionType;
import gr.ntua.softeng.gm.SimpleGoalModelNode;
import gr.ntua.softeng.gm.reasoning.wfr.LowHighNodeValue;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.utils.SystemData;

public class ModelA extends ExtendedModel<SimpleGoalModelNode> {

	private static final ContextKey nameKey = new ContextKey(UserData.class.getName(), "name");
	private static final ContextKey monthKey = new ContextKey(SystemData.class.getName(), "month");
	
	public ModelA(String name){
		super(name);
		
		this.addContextKey(nameKey);
		this.addContextKey(monthKey);

		Affordance affordance = new Affordance("http://www.google.com/KostasAndFebruary", "affordance");
		this.setAffordance(affordance);
		
		SimpleGoalModelNode rootNode = new SimpleGoalModelNode("root");
		List<SimpleGoalModelNode> childNodes = new ArrayList<>();
		childNodes.add(new SimpleGoalModelNode("name"));
		childNodes.add(new SimpleGoalModelNode("month"));
		this.addDecomposition(rootNode, childNodes, DecompositionType.AND);
	}
	
	@Override
	public Map<String, LowHighNodeValue> getInitialValues(AffordanceContext context){
		
		double val1 = 0, val2 = 0;

		ContextValue contextValue = context.getValue(nameKey);
		if (contextValue != null &&
				"Kostas".equals(contextValue.getValue())){
			val1 = 50;
		}

		try{
			DateTimeFormatter formatter= new DateTimeFormatterBuilder()
					.parseCaseInsensitive()
					.appendPattern("MMMM-d-yy")
					.toFormatter()
					.withLocale(Locale.US);

			LocalDate date = LocalDate.parse("january-1-00", formatter);

			contextValue = context.getValue(monthKey);
			if (contextValue != null &&
					date.getMonth().compareTo((Month) contextValue.getValue()) == 0){
				val2 = 50;
			} 
		} catch (DateTimeParseException e){
			val2 = 0;
		}
		

		// Initial Values - Fuzzification
		Map<String, LowHighNodeValue> initialValues = new HashMap<>();
		initialValues.put("name", valueHelper.getValue(val1));
		initialValues.put("month",  valueHelper.getValue(val2));
		
		return initialValues;
	}

}
