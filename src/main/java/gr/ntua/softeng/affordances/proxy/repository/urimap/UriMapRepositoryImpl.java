package gr.ntua.softeng.affordances.proxy.repository.urimap;

import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.affordances.proxy.repository.entities.UriMap;
import gr.ntua.softeng.affordances.proxy.repository.ExtendedRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class UriMapRepositoryImpl implements ExtendedRepository<UriMap> {

	@Autowired
	private UriMapRepository uriMapRepository;
	
	@Override
	public UriMap saveOrUpdate(UriMap uriMap) throws EntityValidationException {
		uriMap = uriMap.validated();
		UriMap foundMap = uriMapRepository.findUriMapByServiceNameAndResourceNameAndHttpMethod(
											uriMap.getServiceName(), uriMap.getResourceName(), uriMap.getHttpMethod());
		if (foundMap != null){
			uriMap.setId(foundMap.getId());
		}
		return uriMapRepository.save(uriMap);
	}
}