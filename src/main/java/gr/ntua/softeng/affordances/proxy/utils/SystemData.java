package gr.ntua.softeng.affordances.proxy.utils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

public class SystemData {

    private static final String[] season = {"Winter", "Winter", "Spring", "Spring", "Spring", "Summer", "Summer",
                                            "Summer", "Fall", "Fall", "Fall", "Winter"};

    public static LocalTime getTime() {
        return LocalTime.now();
    }

    public static Month getMonth(){
        return LocalDate.now().getMonth();
    }

    public static int getYear() {
        return LocalDate.now().getYear();
    }

    public static String getSeason() {
        return season[LocalDate.now().getMonthValue() - 1];
    }
}
