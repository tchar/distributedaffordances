package gr.ntua.softeng.affordances.proxy.proxy.api.resttemplate;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import gr.ntua.softeng.affordances.proxy.proxy.InternalServerErrorException;
import gr.ntua.softeng.affordances.proxy.proxy.ServiceUnavailableException;
import gr.ntua.softeng.affordances.proxy.proxy.api.mapper.ExtendedResourceMapper;
import gr.ntua.softeng.affordances.proxy.repository.entities.HttpHeader;
import gr.ntua.softeng.affordances.proxy.utils.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import gr.ntua.softeng.affordances.framework.mapper.ResourceMapper;
import gr.ntua.softeng.affordances.proxy.resource.ResourceFactory;

@Component("restTemplateFactory")
public class RestTemplateFactory {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(RestTemplateFactory.class);

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;
	
	@Autowired
	private SystemUtils systemUtils;

	@Autowired
	private ResourceFactory resourceFactory;

	public RestTemplate restTemplate(HttpServletRequest request, ResourceMapper mapper){
//		RestTemplateProxyCustomizer customizer = new RestTemplateProxyCustomizer(mapper);
		return restTemplateBuilder.messageConverters(systemUtils.getConverters())
								  .rootUri(mapper.getTranslatedBaseAddress())
								  .errorHandler(new RestTemplateErrorHandler())
//								  .additionalCustomizers(customizer)
								  .build();
	}
	
	public ResponseEntity<Resource<Object>> responseGET(HttpServletRequest request,
														ExtendedResourceMapper mapper)
															throws RestTemplateFactoryException{
		RestTemplate restTemplate = restTemplate(request, mapper);
		return responseGET(request, restTemplate, mapper);
	}
	
	public ResponseEntity<Resource<Object>> responseGET(HttpServletRequest request, 
										RestTemplate restTemplate,ExtendedResourceMapper mapper)
														throws RestTemplateFactoryException{

		ResponseEntity<Resource<Object>> re;
		try{
			re = restTemplate.exchange(mapper.getTranslatedPath(),
							  HttpMethod.valueOf(request.getMethod()),
					   		  createHttpEntity(request, mapper),
							  resourceFactory.getResourceParameterizedTypeReferece(mapper.getMapResourceClassName()));
			
			
		} catch (ClassNotFoundException|IllegalArgumentException e){
			throw new InternalServerErrorException(e.getMessage(), request, mapper);
		} catch (RestClientException e){
			throw new ServiceUnavailableException("Service did not respond", request, mapper);
		}
		return re;
	}

	private HttpEntity createHttpEntity(HttpServletRequest request, ExtendedResourceMapper mapper){
		HttpHeaders headers = getHeaders(request, mapper);
		try {
			Object body = systemUtils.getHttpServletRequestBody(request);
			return new HttpEntity<>(body, headers);
		} catch (IOException e) {
			LOGGER.info(e.getMessage());
			return new HttpEntity(headers);
		}
	}

	private HttpHeaders getHeaders(HttpServletRequest request, ExtendedResourceMapper mapper){
		HttpHeaders headers = DefaultRequestEntities.getDefaultHeaders();
		DefaultRequestEntities.setHeadersFromRequest(headers, request);

		for (HttpHeader header: mapper.getHttpHeaders()){
			headers.set(header.getName(), header.getValue());
		}

		return headers;
	}
	
}
