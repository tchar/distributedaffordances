package gr.ntua.softeng.affordances.proxy.proxy;

import gr.ntua.softeng.affordances.proxy.proxy.api.mapper.ExtendedResourceMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerErrorException extends ProxyException {

    public InternalServerErrorException(String message, HttpServletRequest request, ExtendedResourceMapper mapper) {
        super(message, request, mapper);
    }
}
