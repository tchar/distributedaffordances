package gr.ntua.softeng.affordances.proxy.evaluator.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import de.escalon.hypermedia.affordance.Affordance;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.evaluator.models.Model;
import gr.ntua.softeng.affordances.framework.evaluator.models.ModelFactory;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.affordances.proxy.repository.goalmodel.GoalModelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.proxy.repository.entities.GoalModel;

public class ExtendedModelFactory implements ModelFactory<Affordance>{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ExtendedModelFactory.class);

	private static ExtendedModelFactory instance;
	
	@Autowired
	private GoalModelRepository goalModelRepository;
	
	private ReadWriteLock l;
	
	private Set<Model<?, Affordance>> models;
	private Map<ContextKey, Set<Model<?, Affordance>>> modelMap;

	private boolean initd = false;
	
	private ExtendedModelFactory(){
		l = new ReentrantReadWriteLock();
	}
	
	public static ExtendedModelFactory getInstance(){
		if (instance == null){
			instance = new ExtendedModelFactory();
		}
		return instance;
	}
	
	private void init(){
		List<Model<?, Affordance>> tempModels = new LinkedList<>();

		for (GoalModel gm : goalModelRepository.findAll()) {
			tempModels.add(gm.getConverter().convert());
		}
		
		l.writeLock().lock();
		try{
			modelMap = new HashMap<>();
			models = new HashSet<>();
			for (Model<?, Affordance> model: tempModels){
				addModelUnlocked(model);
			}			
		} finally{
			initd = true;
			l.writeLock().unlock();
		}
	}
	
	public void checkInit(){
		l.readLock().lock();
		if (!initd){
			l.readLock().unlock();
			init();
		} else {
			l.readLock().unlock();
		}
	}

	public Set<Model<?, Affordance>> getModels(AffordanceContext context) {
//		checkInit();

		Set<Model<?, Affordance>> retModels = new HashSet<>();
		l.readLock().lock();
		try{
			retModels.addAll(models);

			LOGGER.debug("All models are: " + retModels);

			// TODO improve this to return models by relevance.

			filterModels(retModels, context);

			LOGGER.debug("Filtered models are: " + retModels);
		}finally{
			l.readLock().unlock();
		}

		return retModels;
	}

	private void preFilterModels(Set<Model<?, Affordance>> models, AffordanceContext context){
		LOGGER.debug("Affordance context \"" + context.getKeys() + "\".");

		Iterator<Model<?, Affordance>> it = models.iterator();
		while (it.hasNext()){
			Model<?, Affordance> model = it.next();
			if (!context.containsAllKeys(model.getContextKeys())){
				LOGGER.debug("Filtering out \"" + model
						+ "\" with ContextKeys \"" + model.getContextKeys() + "\"");
				it.remove();
			}
		}
	}

	private void filterModels(Set<Model<?, Affordance>> models, AffordanceContext context){
		preFilterModels(models, context);

		LOGGER.debug("Cnf list is \"" + context.cnfToString() + "\".");

		Iterator<Model<?, Affordance>> it = models.iterator();
		while (it.hasNext()){
			Model<?, Affordance> model = it.next();
			if (!context.satisfiesCnf(model.getContextKeys())){
				LOGGER.debug("Filtering out \"" + model
						+ "\" with ContextKeys \"" + model.getContextKeys() + "\"");
				it.remove();
			}
		}
	}
	
	private void addModelUnlocked(Model<?, Affordance> model){
		for (ContextKey s: model.getContextKeys()){
			Set<Model<?, Affordance>> set = modelMap.computeIfAbsent(s, k -> new HashSet<>());
			set.remove(model);
			set.add(model);
		}
		models.remove(model);
		models.add(model);
	}
	
	public void addModel(GoalModel model)
			throws DataAccessException, EntityValidationException {
		l.writeLock().lock();
		try{
			model = goalModelRepository.saveOrUpdate(model);
			addModelUnlocked(model.getConverter().convert());
		} finally {
			l.writeLock().unlock();
		}
	}

	public void addMockupModel(Model<?, Affordance> model) {
		l.writeLock().lock();
		try{
			addModelUnlocked(model);
		} finally {
			l.writeLock().unlock();
		}
	}
	
	private void deleteModelUnlocked(Model<?, Affordance> model){
		for (ContextKey s: model.getContextKeys()){
			Set<Model<?, Affordance>> set = modelMap.get(s);
			if (set != null)
				set.remove(model);
		}
		models.remove(model);
	}

	public void deleteMockupModel(Model<?, Affordance> model) {
		l.writeLock().lock();
		try{
			deleteModelUnlocked(model);
		} finally{
			l.writeLock().unlock();
		}
	}
	
	public void deleteModel(String modelName)
			throws DataAccessException, IllegalArgumentException{
		l.writeLock().lock();
		try{
			GoalModel gm = goalModelRepository.findByName(modelName);
			goalModelRepository.delete(gm);
			deleteModelUnlocked(gm.getConverter().convert());
		} finally{
			l.writeLock().unlock();
		}
	}
	
	
	public Set<GoalModel> getAllModels(){
		Set<GoalModel> ret = new HashSet<>();
		l.readLock().lock();
		try{
			for (GoalModel goalModel : goalModelRepository.findAll()) {
				ret.add(goalModel);
			}
		} finally{
			l.readLock().unlock();
		}
		return ret;
	}
}
