package gr.ntua.softeng.affordances.proxy.resource;

import java.util.*;

import gr.ntua.softeng.affordances.proxy.repository.entities.UriMap;
import gr.ntua.softeng.affordances.proxy.repository.entities.GoalModel;

public class JsonResource {

	private String Message;
	private Object data;
	
	public JsonResource(){
	}
	
	public String getMessage() {
		return Message;
	}
	
	public void setMessage(String Message) {
		this.Message = Message;
	}
	
	public Object getData() {
		return data;
	}
	
	public void setData(Object object) {
		this.data = object;
	}
	
	public static class UriMapData{
		
		private List<UriMap> maps;
		private Set<String> resources;
		
		public UriMapData(){
			maps = new LinkedList<>();
		}
		
		public List<UriMap> getMaps() {
			return maps;
		}
		
		public void addMap(UriMap map){
			maps.add(map);
		}
		
		public void addMaps(Iterable<UriMap> maps) {
			for (UriMap map : maps) {
				this.addMap(map);
			}
		}
		
		public Set<String> getResources() {
			return resources;
		}
		
		public void setResources(Set<String> set) {
			this.resources = set;
		}
	}
	
	public static class GoalModelData {
		
		private Set<GoalModel> goalModels;
		private Map<String, Map<String, List<String>>> contextMap;
		private Set<String> externalContextKeys;
		
		public GoalModelData() {
			goalModels = new HashSet<>();
			contextMap = new HashMap<>();
			externalContextKeys = new HashSet<>();
		}
		
		public void setContextMap(Map<String, Map<String, List<String>>> contextMap){
			this.contextMap = contextMap;
		}
		
		public Map<String, Map<String, List<String>>> getContextMap(){
			return contextMap;
		}
		
		public void addGoalModel(GoalModel goalModel){
			this.goalModels.add(goalModel);
		}
		
		public void setGoalModels(Set<GoalModel> goalModels){
			this.goalModels = goalModels;
		}
		
		public Set<GoalModel> getGoalModels(){
			return this.goalModels;
		}

		public Set<String> getExternalContextKeys() {
			return externalContextKeys;
		}

		public void setExternalContextKeys(Set<String> externalContextKeys) {
			this.externalContextKeys = externalContextKeys;
		}
	}
	
}
