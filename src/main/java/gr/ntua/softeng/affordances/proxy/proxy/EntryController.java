package gr.ntua.softeng.affordances.proxy.proxy;

import javax.servlet.http.HttpServletRequest;

import gr.ntua.softeng.affordances.proxy.proxy.api.ApiController;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import de.escalon.hypermedia.spring.AffordanceBuilder;

@Controller
public class EntryController {

	@RequestMapping(value="/")
	public ResponseEntity<Resource<?>> get(HttpServletRequest request){
		
		Resource<?> resource = new Resource<>(new Object());
		resource.add(AffordanceBuilder.linkTo(
				AffordanceBuilder.methodOn(ApiController.class).get(request, null)).withRel("next"));
		return ResponseEntity.ok().body(resource);
	}
	
}
