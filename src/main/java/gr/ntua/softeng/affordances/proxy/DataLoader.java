package gr.ntua.softeng.affordances.proxy;

import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.affordances.proxy.repository.entities.UriMap;
import gr.ntua.softeng.affordances.proxy.repository.urimap.UriMapRepository;
import gr.ntua.softeng.affordances.proxy.resource.external.Order;
import gr.ntua.softeng.affordances.proxy.evaluator.models.ExtendedModelFactory;
import gr.ntua.softeng.affordances.proxy.evaluator.models.ModelB;
import gr.ntua.softeng.affordances.proxy.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import gr.ntua.softeng.affordances.proxy.evaluator.models.ModelA;
import gr.ntua.softeng.affordances.proxy.repository.entities.User;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserRole;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements ApplicationRunner{

	@Autowired
	private UriMapRepository uriMapRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ExtendedModelFactory extendedModelFactory;

	@Override
	public void run(ApplicationArguments arg0) throws Exception {
		addMaps();
		addUsers();
	
		extendedModelFactory.checkInit();
		addModels();
	}
	
	private void addModels(){
		extendedModelFactory.addMockupModel(new ModelA("ModelA"));
		extendedModelFactory.addMockupModel(new ModelB("ModelB"));
	}
	
	private void addMaps() throws EntityValidationException {
		UriMap uriMap = new UriMap("/ebay/order", "http://localhost:8090",
									"/other-api/order", Order.class.getName(), UriMap.HttpMethod.GET);
		uriMapRepository.saveOrUpdate(uriMap);
	}
	
	private void addUsers() throws EntityValidationException {
		User user;
		UserData userData;

		user = new User("john", "john", UserRole.USER);
		userData = new UserData();
		userData.setBirthDate(LocalDate.of(2000, 2, 1));
		userData.setName("Johnas");
		Set<String> interests = new HashSet<>(Collections.singletonList("gadgets"));
		userData.setInterests(interests);
		user.setUserData(userData);
		userRepository.saveOrUpdate(user);

		user = new User("kostas", "kostas", UserRole.ADMIN);
		userData = new UserData();
		userData.setBirthDate(LocalDate.of(1960, 5, 12));
		userData.setName("Kostas");
		user.setUserData(userData);
		userRepository.saveOrUpdate(user);
	}
	
}