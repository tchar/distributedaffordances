package gr.ntua.softeng.affordances.proxy.proxy.api.mapper;

import gr.ntua.softeng.affordances.proxy.ServerConfig;
import gr.ntua.softeng.affordances.proxy.repository.entities.HttpHeader;
import gr.ntua.softeng.affordances.proxy.repository.entities.UriMap;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.List;

public class UriMapMapper implements ExtendedResourceMapper {
	
	private UriMap uriMap;
	private HttpServletRequest request;

	public UriMapMapper(UriMap uriMap, HttpServletRequest request){
		this.uriMap = uriMap;
		this.request = request;
	}

	// TODO replace with uriMap.getOriginalPath
	@Override
	public String getOriginalPath() {
		AntPathMatcher matcher = new AntPathMatcher();
		String pattern = ServerConfig.baseApiPath + "/**";
		String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		return matcher.extractPathWithinPattern(pattern, path);
	}

	@Override
	public String getTranslatedBaseAddress(){
		return uriMap.getForwardBaseAddress();
	}

	private String pathWithParams(String basePath){
		UriComponentsBuilder builder = UriComponentsBuilder.fromPath(basePath);
		Enumeration<String> names = request.getParameterNames();
		while(names.hasMoreElements()){
			String name = names.nextElement();
			builder.queryParam(name, request.getParameter(name));
		}
		return builder.build().encode().toUriString();
	}

	@Override
	public String getTranslatedPath() {
		AntPathMatcher matcher = new AntPathMatcher();
		String pattern = matcher.combine(ServerConfig.baseApiPath, uriMap.getOriginalPath());
		pattern = matcher.combine(pattern, "/**");
		String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

		String internalPath = matcher.extractPathWithinPattern(pattern, path);
		String translatedPath = matcher.combine(uriMap.getForwardPath(), internalPath);
		return pathWithParams(translatedPath);
	}

	@Override
	public String getTranslatedFullPath() {
		return uriMap.getForwardBaseAddress() + getTranslatedPath();
	}

	@Override
	public String getMapResourceClassName() {
		return uriMap.getResourceClassName();
	}

	@Override
	public String getOriginalBaseAddress() {
		return ServerConfig.baseAddress;
	}

	@Override
	public List<HttpHeader> getHttpHeaders() {
		return uriMap.getHttpHeaders();
	}

	@Override
	public UriMap.HttpMethod getHttpMethod() {
		return uriMap.getHttpMethod();
	}

	@Override
	public String getServiceName() {
		return uriMap.getServiceName();
	}
}
