package gr.ntua.softeng.affordances.proxy.annotator.context.external;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManagerException;
import gr.ntua.softeng.affordances.proxy.resource.external.Order;

import javax.annotation.Nullable;
import java.util.*;

public class OrderContextManager implements ContextManager{

    private static final Class<Order> resourceClass = Order.class;
    private static final List<String> fields = Arrays.asList("name", "cost");
    private static Map<String, List<String>> fieldOpTypes = new HashMap<String, List<String>>(){{
        put("name", new LinkedList<>(Collections.singletonList("EQ")));
        put("cost", new LinkedList<>(Arrays.asList("EQ", "LT", "LE", "GT", "GE")));
    }};

    @Override
    public Class<?> getResourceClass() {
        return resourceClass;
    }

    @Override
    public List<String> getResourceFields() {
        return fields;
    }

    @Override
    public List<String> getResourceFieldOperations(String field) {
        return fieldOpTypes.get(field);
    }

    @Override
    public void populate(AffordanceContext context, @Nullable Object object) throws ContextManagerException {

        if (object == null){
            throw new ContextManagerException("Data is null");
        }

        Order order;
        try{
            order = (Order) object;
        } catch (ClassCastException e){
            throw new ContextManagerException("Data is not of type \"" + resourceClass + "\".");
        }

        for (ContextKey key: context.getKeys()) {
            Class<?> resourceClass;
            try {
                resourceClass = Class.forName(key.getResourceName());
            } catch (ClassNotFoundException ignored) {
                throw new ContextManagerException("Did not find class for \"" + key.getResourceName() + "\".");
            }


            if (!resourceClass.equals(this.getResourceClass())){
                continue;
            }

            switch(key.getResourceField()){
                case "name":
                    context.put(key, new ContextValue(order.getName()));
                    break;
                case "cost":
                    context.put(key, new ContextValue(order.getCost()));
                    break;
            }
        }
    }

    private int actualCompare(ContextKey contextKey, Object val1, String val2) throws Exception{
        switch(contextKey.getResourceField()){
            case "name":
                return ((String) val1).compareTo(val2);
            case "cost":
                return ((Double) val1).compareTo(Double.valueOf(val2));
        }
        throw new ContextManagerException("Field \"" + contextKey.getResourceField()
                + "\" cannot be compared by \"" + this.getClass().getSimpleName() + "\"");
    }

    @Override
    public int compare(ContextKey contextKey, ContextValue contextValue, String val2) throws ContextManagerException {
        Class<?> resourceClass;
        try{
            resourceClass = Class.forName(contextKey.getResourceName());
        } catch (ClassNotFoundException e){
            throw new ContextManagerException("Could not found class with name \"" + contextKey.getResourceName()+ "\".");
        }


        if (!resourceClass.equals(this.getResourceClass()))
            throw new ContextManagerException("\"" + this.getClass().getSimpleName()
                    + "\" cannot compare values that belong to resource \"" + contextKey.getResourceName() + "\"");

        if (val2 == null){
            throw new ContextManagerException("String value is empty");
        }

        Object val1 = contextValue.getValue();

        try{
            return actualCompare(contextKey, val1, val2);
        } catch (Exception e){
            throw new ContextManagerException(e);
        }
    }
}
