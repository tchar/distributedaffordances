package gr.ntua.softeng.affordances.proxy.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.escalon.hypermedia.spring.hydra.HydraMessageConverter;
import gr.ntua.softeng.affordances.proxy.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

@Component("systemUtils")
public class SystemUtils {

    @Autowired
    @Qualifier("HalConverter")
    private MappingJackson2HttpMessageConverter halConverter;

    @Autowired
    @Qualifier("JsonConverter")
    private MappingJackson2HttpMessageConverter jsonConverter;

    @Autowired
    @Qualifier("XmlConverter")
    private HttpMessageConverter<?> xmlConverter;

    @Autowired
    private HydraMessageConverter hydraConverter;

    private final static Logger LOGGER = LoggerFactory.getLogger(SystemUtils.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public Object getHttpServletRequestBody(HttpServletRequest request) throws IOException{
        String body = "";
        BufferedReader reader = request.getReader();
        int read;
        int counter = 0;
        while ((read = reader.read()) != -1 && counter++ < ServerConfig.MAX_REQUEST_CHARACTERS){
            body += Character.toString((char) read);
        }
        if (counter >= ServerConfig.MAX_REQUEST_CHARACTERS){
            throw new IOException("Request body too big");
        }
        LOGGER.debug("Request body:\n" + body);
        return stringToObject(body);
    }

    public Object getClientHttpResponseBody(ClientHttpResponse response) throws IOException {
        String body = "";
        InputStream s = response.getBody();
        int read;
        int counter = 0;
        while ((read = s.read()) != -1 && counter++ < ServerConfig.MAX_REQUEST_CHARACTERS){
            body += Character.toString((char) read);
        }
        if (counter >= ServerConfig.MAX_REQUEST_CHARACTERS){
            throw new IOException("Response body too big");
        }
        LOGGER.debug("Response body: \n" + body);
        return stringToObject(body);
    }

    private Object stringToObject(String s) throws IOException {
        return objectMapper.readValue(s, Object.class);
    }

    public List<HttpMessageConverter<?>> getConverters(){
        List<HttpMessageConverter<?>> converters = new LinkedList<>();
        converters.add(halConverter);
        converters.add(jsonConverter);
        converters.add(hydraConverter);
        converters.add(xmlConverter);
        return converters;
    }
}
