package gr.ntua.softeng.affordances.proxy;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.core.AnnotationRelProvider;
import org.springframework.hateoas.core.DefaultRelProvider;
import org.springframework.hateoas.core.DelegatingRelProvider;
import org.springframework.hateoas.core.EvoInflectorRelProvider;
import org.springframework.hateoas.hal.CurieProvider;
import org.springframework.hateoas.hal.DefaultCurieProvider;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.plugin.core.PluginRegistry;
import org.springframework.plugin.core.config.EnablePluginRegistries;
import org.springframework.util.ClassUtils;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.escalon.hypermedia.spring.HypermediaTypes;
import de.escalon.hypermedia.spring.hydra.HydraMessageConverter;
import de.escalon.hypermedia.spring.hydra.JsonLdDocumentationProvider;
import de.escalon.hypermedia.spring.siren.SirenMessageConverter;
import de.escalon.hypermedia.spring.uber.UberJackson2HttpMessageConverter;
import de.escalon.hypermedia.spring.xhtml.XhtmlResourceMessageConverter;

@Configuration
@EnablePluginRegistries(RelProvider.class)
@ComponentScan({"de.escalon.hypermedia.sample.event", "de.escalon.hypermedia.sample.store"})
public class HydraConfiguration extends WebMvcConfigurerAdapter{

	private static final boolean EVO_PRESENT =
            ClassUtils.isPresent("org.atteo.evo.inflector.English", null);
	
	@Autowired
    private PluginRegistry<RelProvider, Class<?>> relProviderRegistry;
	 @Override
	 public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		 converters.add(hydraMessageConverter());
		 converters.add(sirenMessageConverter());
		 converters.add(halConverter());
		 converters.add(uberConverter());
		 converters.add(xhtmlMessageConverter());
		 converters.add(jsonConverter());
	 }
	 
	 @Bean(name="UberConverter")
	 public HttpMessageConverter<?> uberConverter() {
		 UberJackson2HttpMessageConverter converter = new UberJackson2HttpMessageConverter();
		 converter.setSupportedMediaTypes(Collections.singletonList(HypermediaTypes.UBER_JSON));
		 return converter;
	 }

	 @Bean(name="XmlConverter")
	 public HttpMessageConverter<?> xhtmlMessageConverter() {
		 XhtmlResourceMessageConverter xhtmlResourceMessageConverter = new XhtmlResourceMessageConverter();
		 xhtmlResourceMessageConverter.setStylesheets(
				 Collections.singletonList(
						 "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
				 ));
		 xhtmlResourceMessageConverter.setDocumentationProvider(new JsonLdDocumentationProvider());
		 return xhtmlResourceMessageConverter;
	 }

	@Bean
	public HydraMessageConverter hydraMessageConverter() {
	    return new HydraMessageConverter();
	}
	
	@Bean
	public SirenMessageConverter sirenMessageConverter() {
	    SirenMessageConverter sirenMessageConverter = new SirenMessageConverter();
	    sirenMessageConverter.setRelProvider(new DelegatingRelProvider(relProviderRegistry));
	    sirenMessageConverter.setDocumentationProvider(new JsonLdDocumentationProvider());
	    sirenMessageConverter.setSupportedMediaTypes(Collections.singletonList(HypermediaTypes.SIREN_JSON));
	    return sirenMessageConverter;
	}
	
	
	@Bean(name="JacksonObjectMapper")
	public ObjectMapper jacksonObjectMapper() {
	    ObjectMapper objectMapper = new ObjectMapper();
	    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
	    return objectMapper;
	}
	
	@Bean(name="JsonConverter")
	public MappingJackson2HttpMessageConverter jsonConverter() {
	    MappingJackson2HttpMessageConverter jacksonConverter = new
	            MappingJackson2HttpMessageConverter();
	    jacksonConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.valueOf("application/json")));
	    jacksonConverter.setObjectMapper(jacksonObjectMapper());
	    return jacksonConverter;
	}
	
	@Bean
	public CurieProvider curieProvider() {
	    return new DefaultCurieProvider("ex", new UriTemplate("/webapp/hypermedia-api/rels/{rels}"));
	}
	
	@Bean(name="HalConverter")
	public MappingJackson2HttpMessageConverter halConverter() {
	    CurieProvider curieProvider = curieProvider();
	
	    RelProvider relProvider = new DelegatingRelProvider(relProviderRegistry);
	    ObjectMapper halObjectMapper = new ObjectMapper();
	
	    halObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
	
	    halObjectMapper.registerModule(new Jackson2HalModule());
		        halObjectMapper.setHandlerInstantiator(new
		                Jackson2HalModule.HalHandlerInstantiator(relProvider, curieProvider
		                		, new MessageSourceAccessor(new ResourceBundleMessageSource())));
	
	    MappingJackson2HttpMessageConverter halConverter = new
	            MappingJackson2HttpMessageConverter();
	    halConverter.setSupportedMediaTypes(Collections.singletonList(MediaTypes.HAL_JSON));
	    halConverter.setObjectMapper(halObjectMapper);
	    return halConverter;
	}
	
	@Bean
	RelProvider defaultRelProvider() {
	    return EVO_PRESENT ? new EvoInflectorRelProvider() : new DefaultRelProvider();
	}
	
	@Bean
	@Primary
	RelProvider annotationRelProvider() {
	    return new AnnotationRelProvider();
	}
	
}
