package gr.ntua.softeng.affordances.proxy.repository.goalmodel;

import gr.ntua.softeng.affordances.proxy.repository.ExtendedRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import gr.ntua.softeng.affordances.proxy.repository.entities.GoalModel;


@RepositoryRestResource(exported = false)
public interface GoalModelRepository extends CrudRepository<GoalModel, Long>, ExtendedRepository<GoalModel> {

	// Declare query methods here
	GoalModel findByName(String name);
}
