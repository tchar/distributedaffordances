package gr.ntua.softeng.affordances.proxy;

import gr.ntua.softeng.affordances.framework.evaluator.Evaluator;
import gr.ntua.softeng.affordances.proxy.annotator.ExtendedAffordanceAnnotator;
import gr.ntua.softeng.affordances.proxy.annotator.context.ContextFactory;
import gr.ntua.softeng.affordances.framework.evaluator.AffordanceEvaluator;
import gr.ntua.softeng.affordances.proxy.evaluator.models.ExtendedModelFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import gr.ntua.softeng.affordances.proxy.resource.ResourceFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableAutoConfiguration
public class DistributedAffordancesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistributedAffordancesApplication.class, args);
	}

	@Bean(name = "resourceFactory")
	ResourceFactory resourceFactory(){
		return ResourceFactory.getInstance();
	}

	@Bean(name = "modelFactory")
	ExtendedModelFactory modelFactory(){
		return ExtendedModelFactory.getInstance();
	}

	@Bean(name = "contextFactory")
	ContextFactory contextFactory(){
		return ContextFactory.getInstance();
	}

	@Bean(name = "evaluator")
	Evaluator evaluator(){
		return new AffordanceEvaluator();
	}

	@Bean(name = "annotator")
	@DependsOn(value = {"springContextBridge"})
	ExtendedAffordanceAnnotator annotator(){
		return ExtendedAffordanceAnnotator.getInstance();
	}

	@Bean
	BCryptPasswordEncoder bCryptPasswordEncoder(){
		return new BCryptPasswordEncoder();
	}

}
