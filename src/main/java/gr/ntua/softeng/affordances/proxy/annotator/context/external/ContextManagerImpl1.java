package gr.ntua.softeng.affordances.proxy.annotator.context.external;

import java.util.*;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManagerException;
import gr.ntua.softeng.affordances.proxy.resource.external.Order;

import javax.annotation.Nullable;

public class ContextManagerImpl1 implements ContextManager{
	
	private static final Class<?> resourceClass = Order.class;
	private static final List<String> resourceFields = new LinkedList<>(Collections.singletonList("Some field"));
	
	public ContextManagerImpl1(){
		
	}
	
	@Override
	public void populate(AffordanceContext context, @Nullable Object object)
			throws UnsupportedOperationException, ContextManagerException {
		throw new  UnsupportedOperationException("ContextManagerImpl1 not supported");
	}

	@Override
	public Class<?> getResourceClass() throws UnsupportedOperationException{
		throw new  UnsupportedOperationException("ContextManagerImpl1 not supported");
	}

	@Override
	public List<String> getResourceFields() throws UnsupportedOperationException{
		throw new  UnsupportedOperationException("ContextManagerImpl1 not supported");
	}

	@Override
	public List<String> getResourceFieldOperations(String field) {
		throw new  UnsupportedOperationException("ContextManagerImpl1 not supported");
	}

	@Override
	public int compare(ContextKey contextKey, ContextValue val1, String val2) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("ContextManagerImpl1 not supported");
	}
}
