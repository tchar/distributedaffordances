package gr.ntua.softeng.affordances.proxy.annotator.context;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManagerException;
import gr.ntua.softeng.affordances.proxy.utils.SystemData;

import javax.annotation.Nullable;
import java.time.LocalTime;
import java.time.Month;
import java.util.*;


public class SystemDataContextManager implements ContextManager {

    private static final Class<?> resourceClass = SystemData.class;
    private static List<String> resourceFields = new LinkedList<>(Arrays.asList("time", "season", "month", "year"));
    private static Map<String, List<String>> fieldOpTypes = new HashMap<String, List<String>>(){{
        put("time", new LinkedList<>(Arrays.asList("EQ", "LT", "LE", "GT", "GE")));
        put("season", new LinkedList<>(Arrays.asList("EQ", "LT", "LE", "GT", "GE")));
        put("month", new LinkedList<>(Arrays.asList("EQ", "LT", "LE", "GT", "GE")));
        put("year", new LinkedList<>(Arrays.asList("EQ", "LT", "LE", "GT", "GE")));
    }};


    public SystemDataContextManager(){}


    @Override
    public void populate(AffordanceContext context, @Nullable Object object) throws ContextManagerException {


        for (ContextKey key: context.getKeys()){
            Class<?> resourceClass;
            try{
                resourceClass = Class.forName(key.getResourceName());
            } catch (ClassNotFoundException ignored){
                throw new ContextManagerException("Did not find class for \"" + key.getResourceName() + "\".");
            }


            if (!resourceClass.equals(this.getResourceClass())){
                continue;
            }

            switch(key.getResourceField()){
                case "time":
                    context.put(key, new ContextValue(SystemData.getTime()));
                    break;
                case "season":
                    context.put(key, new ContextValue(SystemData.getSeason()));
                    break;
                case "month":
                    context.put(key, new ContextValue(SystemData.getMonth()));
                    break;
                case "year":
                    context.put(key, new ContextValue(SystemData.getYear()));
                    break;
            }
        }
    }

    @Override
    public Class<?> getResourceClass() {
        return resourceClass;
    }

    @Override
    public List<String> getResourceFields() {
        return resourceFields;
    }

    @Override
    public List<String> getResourceFieldOperations(String field) {
        return fieldOpTypes.get(field);
    }

    private int compareTime(Object val1, String val2){
        LocalTime ldt1 = (LocalTime) val1;
        LocalTime ldt2 = LocalTime.parse(val2).withSecond(0).withNano(0);
        ldt1 = ldt1.withSecond(0).withNano(0);
        return ldt1.compareTo(ldt2);
    }

    private int compareMonth(Object val1, String val2){
        Integer mVal1 = ((Month) val1).getValue();
        Integer mVal2;
        try{
            mVal2 = Integer.parseInt(val2);
        } catch (NumberFormatException e){
            mVal2 = Month.valueOf(val2.toUpperCase()).getValue();
        }
        return mVal1.compareTo(mVal2);
    }

    private int compareInt(Object val1, String val2){
        return ((Integer) val1).compareTo(Integer.valueOf(val2));
    }

    private int compareSeason(Object val1, String val2) throws ContextManagerException {
        Map<String, Integer> seasons = new HashMap<String, Integer>(){{
            put("winter", 1);
            put("spring", 2);
            put("summer", 3);
            put("fall", 4);
        }};
        Integer s1 = seasons.get(((String) val1).toLowerCase());
        Integer s2 = seasons.get(val2.toLowerCase());
        if (s1 == null){
            throw new ContextManagerException("\"" + val1 +  "\" is not a valid season");
        } else if (s2 == null){
            throw new ContextManagerException("\"" + val2 +  "\" is not a valid season");
        }
        return s1.compareTo(s2);
    }

    private int actualCompare(ContextKey contextKey, Object val1, String val2) throws Exception{

        switch(contextKey.getResourceField()){
            case "time":
                return compareTime(val1, val2);
            case "season":
                return compareSeason(val1, val2);
            case "month":
                return compareMonth(val1, val2);
            case "year":
                return compareInt(val1, val2);
        }
        throw new ContextManagerException("Field \"" + contextKey.getResourceField()
                + "\" cannot be compared by \"" + this.getClass().getSimpleName() + "\"");
    }

    @Override
    public int compare(ContextKey contextKey, ContextValue contextValue, String val2) throws ContextManagerException {
        Class<?> resourceClass;
        try{
            resourceClass = Class.forName(contextKey.getResourceName());
        } catch (ClassNotFoundException e){
            throw new ContextManagerException("Could not found class with name \"" + contextKey.getResourceName()+ "\".");
        }

        if (!resourceClass.equals(this.getResourceClass()))
            throw new ContextManagerException("\"" + this.getClass().getSimpleName()
                    + "\" cannot compare values that belong to resource \"" + contextKey.getResourceName() + "\"");

        if (val2 == null){
            throw new ContextManagerException("String value is empty");
        }

        Object val1 = contextValue.getValue();

        try{
            return actualCompare(contextKey, val1, val2);
        } catch (Exception e){
            throw new ContextManagerException(e);
        }
    }
}
