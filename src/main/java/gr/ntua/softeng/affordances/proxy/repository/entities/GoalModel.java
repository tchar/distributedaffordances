package gr.ntua.softeng.affordances.proxy.repository.entities;

import java.util.*;
import java.util.regex.Pattern;

import javax.persistence.*;

import de.escalon.hypermedia.affordance.Affordance;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.evaluator.compare.CompareCalculator;
import gr.ntua.softeng.affordances.framework.evaluator.compare.DefaultCompareCalculator;
import gr.ntua.softeng.affordances.framework.evaluator.compare.DefaultOperation;
import gr.ntua.softeng.affordances.proxy.SpringContextBridge;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.gm.DataGoalModelNode;
import gr.ntua.softeng.affordances.framework.evaluator.models.LeafData;
import gr.ntua.softeng.affordances.proxy.evaluator.models.ExtendedAffordanceModel;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name="GOAL_MODELS")
public class GoalModel implements EntityValidator<GoalModel> {

	private final static Logger LOGGER = LoggerFactory.getLogger(GoalModel.class);
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String affordance;

	@Column
	private String baseResourceClassName;
	
	@OneToOne(optional = false, cascade=CascadeType.ALL)
	private GoalModelNode root;
	
	public GoalModel(){
		
	}

	private List<String> getResourceTypes(){
		List<String> resourceTypes = new LinkedList<>();
		LinkedList<GoalModelNode> nodes = new LinkedList<>();
		nodes.add(root);

		while (!nodes.isEmpty()){
			GoalModelNode node = nodes.pop();
			if (node.getType().equals(GoalModelNode.NodeType.LEAF)){
				resourceTypes.add(node.getResourceType());
			} else {
				nodes.addAll(node.getChildren());
			}
		}
		return resourceTypes;
	}


	private void validate() throws EntityValidationException {
		if (name == null || name.trim().equals(""))
			throw new EntityValidationException("Node name cannot be empty");
		if (affordance == null || affordance.trim().equals(""))
			throw new EntityValidationException("Node type cannot be empty");
		if (root == null)
			throw new EntityValidationException("Root must not be empty");
		
    	String s = "^[a-zA-Z0-9_]+$";
				
		if (!Pattern.compile(s).matcher(name).find())
			throw new EntityValidationException(
						"Invalid model name \"" + name +"\". Example model_1");
		
		root.validated();

		Set<String> acceptableNames = new HashSet<>();
		if (baseResourceClassName != null && !baseResourceClassName.equals("None")){
			acceptableNames.add(baseResourceClassName);
		}
		acceptableNames.addAll(SpringContextBridge.services().contextFactory().getInternalContextKeys());
		for (String name: getResourceTypes()){
			if (!acceptableNames.contains(name)){
				throw new EntityValidationException("Base resource type is \"" + baseResourceClassName +
						" \" and found leaf with resource type \"" + name + "\".");
			}
		}
	}
	
	@Override
	public boolean isValid() {
		try {
			validate();
		} catch (EntityValidationException e){
			return false;
		}
		return true;
	}

	@Override
	public GoalModel validated() throws EntityValidationException {
		validate();
		return this;
	}
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAffordance() {
		return affordance;
	}
	
	public void setAffordance(String affordance) {
		this.affordance = affordance;
	}

	public GoalModelNode getRoot() {
		return root;
	}

	public void setRoot(GoalModelNode root) {
		this.root = root;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBaseResourceClassName(){
		return baseResourceClassName;
	}

	public void setBaseResourceClassName(String baseResourceClassName) {
		this.baseResourceClassName = baseResourceClassName;
	}


	@Transient
	public GoalModelConverter getConverter(){
		return new GoalModelConverter(this);
	}
	public class GoalModelConverter{

		private final GoalModel gm;

		private GoalModelConverter(GoalModel gm){
			this.gm = gm;
		}

		private String getNodeUniqueName(GoalModelNode n) throws IllegalArgumentException{
			if (n.getId() == null){
				String msg = "\"" + n.getName() + "\" has null id";
				LOGGER.error(msg);
				throw new IllegalArgumentException(msg);
			}
			return Long.toString(n.getId());
		}


		private DataGoalModelNode<LeafData> getOrCreateGoalModelNode(
				ExtendedAffordanceModel m, GoalModelNode n) throws IllegalArgumentException{
			String uniqueName = getNodeUniqueName(n);
			DataGoalModelNode<LeafData> node = m.getNodeByName(uniqueName);
			if (node == null)
				node = new DataGoalModelNode<>(uniqueName);
			return node;
		}

		public ExtendedAffordanceModel convert(){
			return convert(new DefaultCompareCalculator());
		}

		public ExtendedAffordanceModel convert(CompareCalculator calculator){
			ExtendedAffordanceModel m = new ExtendedAffordanceModel(gm.getName(), calculator);

			m.setAffordance(new Affordance(gm.getAffordance()));

			LinkedList<GoalModelNode> nodes = new LinkedList<>();
			nodes.add(gm.getRoot());
			while(!nodes.isEmpty()){
				GoalModelNode fatherNode = nodes.pop();
				DataGoalModelNode<LeafData> node = this.getOrCreateGoalModelNode(m, fatherNode);
				// TODO add weight model node
				List<DataGoalModelNode<LeafData>> children = new ArrayList<>();
				if (fatherNode.getType().equals(GoalModelNode.NodeType.NODE)){

					// Add all children to model
					for(GoalModelNode child: fatherNode.getChildren()){
						nodes.add(child);
						children.add(this.getOrCreateGoalModelNode(m, child));
					}
					// Add decomposition type
					m.addDecomposition(node, children, fatherNode.getDecType());
				} else if (fatherNode.getType().equals(GoalModelNode.NodeType.LEAF)){
					LeafData ld = new LeafData();
					ld.setResourceType(fatherNode.getResourceType());
					ld.setResourceField(fatherNode.getResourceField());
					ld.setOp(new DefaultOperation(fatherNode.getOpType().toDefaultOperationType()));
					ld.setValue(fatherNode.getValue());
					ld.setWeight(fatherNode.getWeight());
					node.setData(ld);
					m.addContextKey(new ContextKey(ld.getResourceType(), ld.getResourceField()));
				}
			}
			return m;
		}
	}
}
