package gr.ntua.softeng.affordances.proxy;

import gr.ntua.softeng.affordances.framework.evaluator.Evaluator;
import gr.ntua.softeng.affordances.proxy.annotator.context.ContextFactory;
import gr.ntua.softeng.affordances.proxy.evaluator.models.ExtendedModelFactory;
import gr.ntua.softeng.affordances.proxy.resource.ResourceFactory;
import gr.ntua.softeng.affordances.proxy.utils.SystemUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@DependsOn(value={"modelFactory", "evaluator", "resourceFactory"})
public class SpringContextBridge
        implements SpringContextBridgedServices, ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Autowired
    private SystemUtils systemUtils;

    @Autowired
    private ContextFactory contextFactory;

    @Autowired
    private ExtendedModelFactory modelFactory;

    @Autowired
    private Evaluator evaluator;

    @Autowired
    private ResourceFactory resourceFactory;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        SpringContextBridge.applicationContext = applicationContext;
    }

    /**
     * A static method to lookup the SpringContextBridgedServices Bean in
     * the applicationContext. It is basically an instance of itself, which
     * was registered by the @Component annotation.
     *
     * @return the SpringContextBridgedServices, which exposes all the
     * Spring services that are bridged from the Spring context.
     */
    public static SpringContextBridgedServices services() {
        return applicationContext.getBean(SpringContextBridgedServices.class);
    }

    @Override
    public SystemUtils getSystemUtils() {
        return systemUtils;
    }

    @Override
    public ContextFactory contextFactory() {
        return contextFactory;
    }

    @Override
    public ExtendedModelFactory modelFactory() {
        return modelFactory;
    }

    @Override
    public Evaluator evaluator() {
        return evaluator;
    }

    @Override
    public ResourceFactory resourceFactory() {
        return resourceFactory;
    }
}