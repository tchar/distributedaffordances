package gr.ntua.softeng.affordances.proxy.repository.entities;

import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidator;

import javax.persistence.*;

@Entity
@Table(name="HEADERS")
public class HttpHeader implements EntityValidator<HttpHeader> {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String name;
    private String value;

    public HttpHeader(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean isValid() {
        try{
            validated();
            return true;
        } catch (EntityValidationException ignored){
            return false;
        }
    }

    @Override
    public HttpHeader validated() throws EntityValidationException {
        if (name == null)
            throw new EntityValidationException("header name is null");
        if (value == null)
            throw new EntityValidationException("value is null");

        if (name.trim().equals(""))
            throw new EntityValidationException("name is empty");

        if (value.trim().equals(""))
            throw new EntityValidationException("value is empty");

        return this;
    }
}
