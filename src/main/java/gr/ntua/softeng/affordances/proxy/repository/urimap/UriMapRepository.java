package gr.ntua.softeng.affordances.proxy.repository.urimap;

import gr.ntua.softeng.affordances.proxy.repository.ExtendedRepository;
import gr.ntua.softeng.affordances.proxy.repository.entities.UriMap;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface UriMapRepository extends CrudRepository<UriMap, Long>, ExtendedRepository<UriMap> {

	UriMap findUriMapByServiceNameAndResourceNameAndHttpMethod(
			String serviceName, String resourceName, UriMap.HttpMethod httpMethod);
}