package gr.ntua.softeng.affordances.proxy.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class BackendAdminUsernamePasswordAuthenticationToken  extends UsernamePasswordAuthenticationToken{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5442589540831894203L;

	public BackendAdminUsernamePasswordAuthenticationToken(Object principal, Object credentials) {
		super(principal, credentials);
	}

}
