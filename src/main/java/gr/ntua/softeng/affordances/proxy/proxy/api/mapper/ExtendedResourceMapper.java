package gr.ntua.softeng.affordances.proxy.proxy.api.mapper;

import gr.ntua.softeng.affordances.framework.mapper.ResourceMapper;
import gr.ntua.softeng.affordances.proxy.repository.entities.HttpHeader;
import gr.ntua.softeng.affordances.proxy.repository.entities.UriMap;

import java.util.List;

public interface ExtendedResourceMapper extends ResourceMapper {

    List<HttpHeader> getHttpHeaders();
    UriMap.HttpMethod getHttpMethod();
    String getServiceName();

}
