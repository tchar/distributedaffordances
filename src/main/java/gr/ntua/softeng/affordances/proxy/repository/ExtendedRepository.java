package gr.ntua.softeng.affordances.proxy.repository;

public interface ExtendedRepository<T> {

	T saveOrUpdate(T entity) throws EntityValidationException;
	
}
