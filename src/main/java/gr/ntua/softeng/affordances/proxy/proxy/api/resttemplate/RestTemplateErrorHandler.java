package gr.ntua.softeng.affordances.proxy.proxy.api.resttemplate;

import gr.ntua.softeng.affordances.proxy.SpringContextBridge;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

import java.io.IOException;

public class RestTemplateErrorHandler implements ResponseErrorHandler {



    @Override
    public boolean hasError(ClientHttpResponse response)  {
        try {
            return !response.getStatusCode().is2xxSuccessful();
        } catch (IOException e) {
            return true;
        }
    }

    @Override
    public void handleError(ClientHttpResponse response) throws RestTemplateFactoryException, RestClientException{
        try{
            RestTemplateFactoryException ex = new RestTemplateFactoryException("Response error.");
            ex.setHttpStatus(response.getStatusCode());
            ex.setHttpHeaders(response.getHeaders());
            ex.setResponseData(SpringContextBridge.services()
                    .getSystemUtils().getClientHttpResponseBody(response));
            throw ex;
        } catch (IOException ignored){
            throw new RestClientException("Service unavailable");
        }
    }
}
