package gr.ntua.softeng.affordances.proxy.evaluator.models;

import de.escalon.hypermedia.affordance.Affordance;
import gr.ntua.softeng.gm.GoalModelNode;
import gr.ntua.softeng.affordances.framework.evaluator.models.Model;

import java.util.Objects;

public abstract class ExtendedModel<T extends GoalModelNode> extends Model<T, Affordance>{

    public ExtendedModel(String name) {
        super(name);
    }

    @Override
    public boolean equals(Object o){
        if (o == null) return false;
        if (!(o instanceof Model)) return false;

        Model<?, ?> m = (Model<?, ?>) o;
        return this.name.equals(m.getName());
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.name);
    }

}
