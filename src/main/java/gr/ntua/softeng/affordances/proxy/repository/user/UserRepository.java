package gr.ntua.softeng.affordances.proxy.repository.user;

import gr.ntua.softeng.affordances.proxy.repository.ExtendedRepository;
import gr.ntua.softeng.affordances.proxy.repository.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface UserRepository extends CrudRepository<User, Long>, ExtendedRepository<User> {
	
	User findUserByUsername(String username);
	
	User findUserByUsernameAndPassword(String username, String password);
}
