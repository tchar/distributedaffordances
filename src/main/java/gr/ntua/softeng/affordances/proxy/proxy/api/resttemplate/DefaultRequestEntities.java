package gr.ntua.softeng.affordances.proxy.proxy.api.resttemplate;

import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

public class DefaultRequestEntities {

    public static HttpHeaders getDefaultHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Accepts", "application/json");
        return headers;
    }

    private static void setHeaderIfExists(String headerName, HttpHeaders headers, HttpServletRequest request){
        String headerValue = request.getHeader(headerName);
        if (headerValue != null){
            headers.add(headerName, headerValue);
        }
    }

    public static void setHeadersFromRequest(HttpHeaders headers, HttpServletRequest request){
        setHeaderIfExists("Content-Type", headers, request);
        setHeaderIfExists("Accepts", headers, request);
    }
}
