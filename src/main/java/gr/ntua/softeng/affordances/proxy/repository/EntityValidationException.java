package gr.ntua.softeng.affordances.proxy.repository;

public class EntityValidationException extends Exception {

    private String message;

    public EntityValidationException(String msg){
        super(msg);
        this.message = msg;
    }

    public String getSpecificMessage(){
        return message;
    }
}
