package gr.ntua.softeng.affordances.proxy.security;

import javax.servlet.http.HttpServletResponse;

import gr.ntua.softeng.affordances.proxy.repository.entities.UserRole;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import gr.ntua.softeng.affordances.proxy.ServerConfig;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	

    protected final static String authApiContentType = "application/json";
	protected static final String REALM = "Softlab-Affordances";
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    http.
	            csrf().disable().
	            sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
	            and().
	            authorizeRequests().
	            antMatchers(managementEndpoints()).hasAuthority(UserRole.ADMIN.toString()).
	            antMatchers(apiEndpoints()).hasAuthority(UserRole.USER.toString()).
	            and().
	            anonymous().disable().
	            exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint());

	    http.addFilterBefore(new AuthenticationFilter(authenticationManager()), BasicAuthenticationFilter.class).
	            addFilterBefore(new ManagementEndpointAuthenticationFilter(authenticationManager()), BasicAuthenticationFilter.class);
	}
	

    protected static String[] managementEndpoints() {
    	return new String[] {ServerConfig.baseAdminPath + "/**", "/js/admin/**", "/css/admin/**",
							"/templates/admin/**", "/img/admin/**"};
    }
    
    protected static String[] apiEndpoints(){
    	return new String[] {ServerConfig.baseApiPath + "/**"};
    }
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    auth.authenticationProvider(domainUsernamePasswordAuthenticationProvider()).
	            authenticationProvider(backendAdminUsernamePasswordAuthenticationProvider());
	}
	
    
    @Bean
    public AuthenticationProvider domainUsernamePasswordAuthenticationProvider() {
        return new DomainUsernamePasswordAuthenticationProvider();
    }

    @Bean
    public AuthenticationProvider backendAdminUsernamePasswordAuthenticationProvider() {
        return new BackendAdminUsernamePasswordAuthenticationProvider();
    }

    @Bean
    public AuthenticationEntryPoint unauthorizedEntryPoint() {
        return (request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
