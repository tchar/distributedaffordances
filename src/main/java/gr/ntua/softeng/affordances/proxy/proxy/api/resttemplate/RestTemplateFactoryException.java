package gr.ntua.softeng.affordances.proxy.proxy.api.resttemplate;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

public class RestTemplateFactoryException extends RuntimeException{

	private static final long serialVersionUID = 1427950694077947427L;

	private HttpStatus httpStatus = null;
	private Object responseData = null;
	private HttpHeaders httpHeaders = null;

	public RestTemplateFactoryException(String message) {
		super(message);
	}

    public void setHttpStatus(HttpStatus httpStatus){
		this.httpStatus = httpStatus;
	}

	public void setResponseData(Object responseData){
    	this.responseData = responseData;
	}

	public void setHttpHeaders(HttpHeaders httpHeaders){
		this.httpHeaders = httpHeaders;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public Object getResponseData() {
		return responseData;
	}

	public HttpHeaders getHttpHeaders() {
		return httpHeaders;
	}
}
