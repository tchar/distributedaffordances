package gr.ntua.softeng.affordances.proxy;

import org.springframework.context.annotation.Configuration;

@Configuration
public class ServerConfig {

    public static String baseAddress = "http://localhost:9420";
	public static final String baseAdminPath = "/admin";
	public static final String jsAdminPath = "/js/admin";
	public static final String baseApiPath = "/api";
	public static final String authApiPath = "/auth/api";
	public static final int MAX_REQUEST_CHARACTERS = 10000;
}
