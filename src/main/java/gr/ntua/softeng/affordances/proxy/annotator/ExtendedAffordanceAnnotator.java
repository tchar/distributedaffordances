package gr.ntua.softeng.affordances.proxy.annotator;

import de.escalon.hypermedia.affordance.Affordance;
import gr.ntua.softeng.affordances.framework.annotator.AffordanceAnnotator;
import gr.ntua.softeng.affordances.framework.annotator.policy.PolicyThreshold;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.evaluator.Evaluator;
import gr.ntua.softeng.affordances.framework.evaluator.models.ModelFactory;
import gr.ntua.softeng.affordances.proxy.SpringContextBridge;
import gr.ntua.softeng.affordances.proxy.resource.ApiResource;
import org.springframework.hateoas.Resource;

import java.util.List;

public class ExtendedAffordanceAnnotator extends AffordanceAnnotator<Affordance> {
	
	private static ExtendedAffordanceAnnotator instance;

	private ExtendedAffordanceAnnotator(ModelFactory<Affordance> factory, Evaluator evaluator){
		super(factory, evaluator);
	}

	public static ExtendedAffordanceAnnotator getInstance(){
		if (instance == null){
			ModelFactory<Affordance> modelFactory = SpringContextBridge.services().modelFactory();
			Evaluator evaluator = SpringContextBridge.services().evaluator();
			instance = new ExtendedAffordanceAnnotator(modelFactory, evaluator);
		}
		return instance;
	}

	public void annotate(Resource<ApiResource> resource, AffordanceContext context) {
		List<Affordance> affordances = super.getAffordances(context, new PolicyThreshold(0.0));
		int i = 0;
		for (Affordance affordance: affordances){
			resource.add(affordance.withRel("affordance_" + i++));
		}
	}
}
