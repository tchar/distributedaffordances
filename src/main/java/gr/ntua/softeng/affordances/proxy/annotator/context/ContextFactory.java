package gr.ntua.softeng.affordances.proxy.annotator.context;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.builder.ContextBuilder;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManagerException;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContextFactory {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ContextFactory.class);

	private static ContextFactory instance;
	
	private static final String DEFAULT_PATH = ContextFactory.class.getPackage().getName() + ".external";
	
	private ReadWriteLock l;
	private Map<String, Map<String, List<String>>> contextMap;
	private Set<String> internalContextKeys;
	private Set<String> externalContextKeys;
	private List<ContextBuilder> contextBuilders;
	private Map<String, ContextManager> contextManagers;
	
	private ContextFactory(){
		l = new ReentrantReadWriteLock();
		contextMap = new HashMap<>();
		contextBuilders = new ArrayList<>();
		contextManagers = new HashMap<>();
		internalContextKeys = new HashSet<>();
		externalContextKeys = new HashSet<>();
		init();
	}
	
	public static ContextFactory getInstance(){
		if (instance == null){
			instance = new ContextFactory();
		}
		return instance;
	}

	private Map<String, List<String>> getResourceFieldOperationsMap(ContextManager manager){
		Map<String, List<String>> ret = new HashMap<>();
		for (String field: manager.getResourceFields()){
			List<String> ops = new LinkedList<>(manager.getResourceFieldOperations(field));
			ret.put(field, ops);
		}
		return ret;
	}
	
	private void init(){
		List<ClassLoader> classLoadersList = new LinkedList<>();
		classLoadersList.add(ClasspathHelper.contextClassLoader());
		classLoadersList.add(ClasspathHelper.staticClassLoader());
	
		Reflections reflections = new Reflections(new ConfigurationBuilder()
		    .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
		    .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
		    .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(DEFAULT_PATH))));


		Map<String, Map<String, List<String>>> tempContextMap = new HashMap<>();
		Set<String> tempInternalContextKeys = new HashSet<>();
		Set<String> tempExternalContextKeys = new HashSet<>();
		Map<String, ContextManager> tempManagers = new HashMap<>();
		
		// Add values from UserDataContextManager
		ContextManager manager = new UserDataContextManager();
		tempContextMap.put(manager.getResourceClass().getName(), getResourceFieldOperationsMap(manager));
		tempManagers.put(manager.getResourceClass().getName(), manager);
		tempInternalContextKeys.add(manager.getResourceClass().getName());
		// Add values from SystemDataContextManager
		manager = new SystemDataContextManager();
		tempContextMap.put(manager.getResourceClass().getName(), getResourceFieldOperationsMap(manager));
		tempManagers.put(manager.getResourceClass().getName(), manager);
		tempInternalContextKeys.add(manager.getResourceClass().getName());

		for (Class<?> aClass : reflections.getSubTypesOf(Object.class)) {
			String name = aClass.getName();
			try {
				manager = (ContextManager) Class.forName(name).newInstance();
				tempContextMap.put(manager.getResourceClass().getName(), getResourceFieldOperationsMap(manager));
				tempExternalContextKeys.add(manager.getResourceClass().getName());
				tempManagers.put(manager.getResourceClass().getName(), manager);
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | UnsupportedOperationException e) {
				LOGGER.error(e.getMessage());
			}
		}

		l.writeLock().lock();
		try{
			contextMap = tempContextMap;
			contextManagers = tempManagers;
			internalContextKeys = tempInternalContextKeys;
			externalContextKeys = tempExternalContextKeys;
		} finally {
			l.writeLock().unlock();
		}
		contextBuilders.addAll(Collections.singletonList(new ContextBuilderImpl()));
	}
	
	// For example purposes
	private ContextBuilder getRandomContextBuilder(){
		ContextBuilder builder = contextBuilders.get(new Random().nextInt(contextBuilders.size()));
		LOGGER.debug("Using context builder" + builder.getClass().getSimpleName());
		return builder;
	}

	public AffordanceContext getContext(ContextBuilder builder, Object... data) throws ContextManagerException {
		return builder.build().populate(data);
	}


	public AffordanceContext getContext(Object... data) throws ContextManagerException {
		ContextBuilder builder = new OrderContextBuilder1();
		builder.setManagers(contextManagers.values());
		return getContext(builder, data);
	}

	// DefaultContext
	public AffordanceContext getDefaultContext(String resourceClassName, Object... data) throws ContextManagerException {
		List<ContextManager> managers = new LinkedList<>();
		for (Object obj: data){
			String name = obj.getClass().getName();
			ContextManager manager = contextManagers.get(name);
			if (manager == null){
				throw new ContextManagerException("No context manager found for \"" + name + "\".");
			}
			managers.add(manager);
		}
		return getContext(new ResourceDefaultContextBuilder(managers), data);
	}

	public Map<String, Map<String, List<String>>> getContextMap(){
		return this.contextMap;
	}

	public Collection<String> getResourceFields(String resourceName){
		return this.contextMap.get(resourceName).keySet();
	}
	
	public void reload(){
		init();
	}

	public Set<String> getExternalContextKeys() {
		return externalContextKeys;
	}

	public Set<String> getInternalContextKeys() {
		return internalContextKeys;
	}
}
