package gr.ntua.softeng.affordances.proxy.proxy;

import gr.ntua.softeng.affordances.proxy.proxy.api.mapper.ExtendedResourceMapper;

import javax.servlet.http.HttpServletRequest;

public class ServiceUnavailableException extends ProxyException {

    public ServiceUnavailableException(String message, HttpServletRequest request, ExtendedResourceMapper mapper) {
        super(message, request, mapper);
    }
}
