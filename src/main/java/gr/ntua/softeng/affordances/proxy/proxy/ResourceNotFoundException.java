package gr.ntua.softeng.affordances.proxy.proxy;

import gr.ntua.softeng.affordances.proxy.proxy.api.mapper.ExtendedResourceMapper;

import javax.servlet.http.HttpServletRequest;

public class ResourceNotFoundException extends ProxyException {

	public ResourceNotFoundException(String message, HttpServletRequest request, ExtendedResourceMapper mapper) {
		super(message, request, mapper);
	}

}