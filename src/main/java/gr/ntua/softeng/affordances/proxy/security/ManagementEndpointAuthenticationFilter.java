package gr.ntua.softeng.affordances.proxy.security;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.util.UrlPathHelper;

public class ManagementEndpointAuthenticationFilter extends GenericFilterBean{

    private final static Logger LOGGER = LoggerFactory.getLogger(ManagementEndpointAuthenticationFilter.class);
    private AuthenticationManager authenticationManager;
    private Set<String> baseManagementEndpoints;

    public ManagementEndpointAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        this.baseManagementEndpoints = new HashSet<>(Arrays.asList(SecurityConfig.managementEndpoints()));
    }
	
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = asHttp(request);
        HttpServletResponse httpResponse = asHttp(response);

        String resourcePath = new UrlPathHelper().getPathWithinApplication(httpRequest);
        try {
            if (requestToManagementEndpoints(resourcePath)) {
                processManagementEndpointAuthorizationHeader(httpRequest, httpResponse);
            }

            LOGGER.debug("ManagementEndpointAuthenticationFilter is passing request down the filter chain");
            chain.doFilter(request, response);
        } catch (AuthenticationException authenticationException) {
            SecurityContextHolder.clearContext();
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, authenticationException.getMessage());
        }
    }

    private HttpServletRequest asHttp(ServletRequest request) {
        return (HttpServletRequest) request;
    }
    
    private HttpServletResponse asHttp(ServletResponse response) {
        return (HttpServletResponse) response;
    }
    
    private boolean requestToManagementEndpoints(String resourcePath) {
    	AntPathMatcher matcher = new AntPathMatcher();
    	boolean matched = false;
    	for (String pattern: this.baseManagementEndpoints){
    		matched = matched || matcher.match(pattern, resourcePath);
    	}
    	return matched;
    }
    
    private String[] getUserNameAndPasswordAuthorizationHeader(String auth){
    	String[] ret = new String[2];
    	try{
            if (auth.startsWith("Basic")) {
                // Authorization: Basic base64credentials
                String base64Credentials = auth.substring("Basic".length()).trim();
                String credentials = new String(Base64.getDecoder().decode(base64Credentials),
                        Charset.forName("UTF-8"));
                // credentials = username:password
                ret = credentials.split(":", 2);
                } 
         } catch (IllegalArgumentException|IndexOutOfBoundsException e){
            	LOGGER.debug(e.getMessage());
    	}
    	return ret;
    }
    
    private void processManagementEndpointAuthorizationHeader(HttpServletRequest httpRequest,
    											HttpServletResponse httpResponse) throws IOException{
        String authorization = httpRequest.getHeader("Authorization");
    	if (authorization != null){
            String[] vals = getUserNameAndPasswordAuthorizationHeader(authorization);
            String username = vals[0];
            String password = vals[1];
            processManagementEndpointUsernamePasswordAuthentication(username, password);
        } else {
        	httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            httpResponse.addHeader("WWW-Authenticate", "Basic realm=" + SecurityConfig.REALM);
        }
    }
    
    private void processManagementEndpointUsernamePasswordAuthentication(String username, String password) throws IOException {
        LOGGER.debug("Trying to authenticate user {} for management endpoint by Authorization method", username);
    	Authentication resultOfAuthentication = tryToAuthenticateWithUsernameAndPassword(username, password);
        SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
    }
    
    private Authentication tryToAuthenticateWithUsernameAndPassword(String username, String password) {
        BackendAdminUsernamePasswordAuthenticationToken requestAuthentication = new BackendAdminUsernamePasswordAuthenticationToken(username, password);
        return tryToAuthenticate(requestAuthentication);
    }
    
    private Authentication tryToAuthenticate(Authentication requestAuthentication) {
        Authentication responseAuthentication = authenticationManager.authenticate(requestAuthentication);
        if (responseAuthentication == null || !responseAuthentication.isAuthenticated()) {
            throw new InternalAuthenticationServiceException("Unable to authenticate Backend Admin for provided credentials");
        }
        LOGGER.debug("Backend Admin successfully authenticated");
        return responseAuthentication;
    }
}
