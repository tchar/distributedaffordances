package gr.ntua.softeng.affordances.proxy.evaluator.models;

import de.escalon.hypermedia.affordance.Affordance;
import gr.ntua.softeng.affordances.framework.evaluator.compare.CompareCalculator;
import gr.ntua.softeng.affordances.framework.evaluator.models.AffordanceModel;
import gr.ntua.softeng.affordances.framework.evaluator.models.Model;

import java.util.Objects;

public class ExtendedAffordanceModel extends AffordanceModel<Affordance> {

    public ExtendedAffordanceModel(String name, CompareCalculator calculator) {
        super(name, calculator);
    }

    @Override
    public boolean equals(Object o){
        if (o == null) return false;
        if (!(o instanceof Model)) return false;

        Model<?, ?> m = (Model<?, ?>) o;
        return this.name.equals(m.getName());
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.name);
    }
}
