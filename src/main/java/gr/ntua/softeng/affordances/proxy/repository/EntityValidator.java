package gr.ntua.softeng.affordances.proxy.repository;

public interface EntityValidator<T> {

	boolean isValid();
	T validated() throws EntityValidationException;
}
