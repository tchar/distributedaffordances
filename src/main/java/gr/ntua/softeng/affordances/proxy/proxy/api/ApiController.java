package gr.ntua.softeng.affordances.proxy.proxy.api;

import de.escalon.hypermedia.spring.AffordanceBuilder;
import gr.ntua.softeng.affordances.proxy.ServerConfig;
import gr.ntua.softeng.affordances.proxy.proxy.api.mapper.ExtendedResourceMapper;
import gr.ntua.softeng.affordances.proxy.proxy.api.resttemplate.RestTemplateFactory;
import gr.ntua.softeng.affordances.proxy.proxy.api.resttemplate.RestTemplateFactoryException;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.proxy.annotator.ExtendedAffordanceAnnotator;
import gr.ntua.softeng.affordances.proxy.annotator.context.ContextFactory;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManagerException;
import gr.ntua.softeng.affordances.framework.mapper.MapperException;
import gr.ntua.softeng.affordances.proxy.proxy.InternalServerErrorException;
import gr.ntua.softeng.affordances.proxy.proxy.ResourceNotFoundException;
import gr.ntua.softeng.affordances.proxy.proxy.ServiceUnavailableException;
import gr.ntua.softeng.affordances.proxy.proxy.api.mapper.MapperFactory;
import gr.ntua.softeng.affordances.proxy.repository.DatabaseMigrator;
import gr.ntua.softeng.affordances.proxy.repository.entities.UriMap;
import gr.ntua.softeng.affordances.proxy.repository.entities.User;
import gr.ntua.softeng.affordances.proxy.resource.ApiResource;
import gr.ntua.softeng.affordances.proxy.resource.ErrorResource;
import gr.ntua.softeng.affordances.proxy.resource.ResourceFactory;
import gr.ntua.softeng.affordances.proxy.security.CurrentlyLoggedUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

//import static de.escalon.hypermedia.spring.AffordanceBuilder.methodOn;
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/*
	Main Controller that handles all api requests.
 */
@Controller
@RequestMapping(ServerConfig.baseApiPath)
public class ApiController {

	private final static Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

	@Autowired
	private MapperFactory mapperFactory;

	@Autowired
	private RestTemplateFactory restTemplateFactory;

	@Autowired
	private ResourceFactory resourceFactory;

	@Autowired
	private ExtendedAffordanceAnnotator annotator;

	@Autowired
	private DatabaseMigrator databaseMigrator;

	@Autowired
	private ContextFactory contextFactory;

	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity<Resource<?>> get(HttpServletRequest request, @CurrentlyLoggedUser User user){

		List<UriMap> maps = mapperFactory.getAllMaps();
		Resource<?> resource = new Resource<>(new Object());
		for (UriMap map: maps){
			String rel = map.getResourceClassName();
			int ind = rel.lastIndexOf(".");
			if (ind != -1)
				rel = rel.substring(ind + 1);
			resource.add(AffordanceBuilder.linkTo(ApiController.class)
					.slash(map.getOriginalPath()).slash("").rel(rel).build());
		}
		return ResponseEntity.ok().body(resource);

	}

	@RequestMapping(value = "/{serviceName}/{resourceName}/**")
	@ResponseBody
	public ResponseEntity<Resource<ApiResource>> getMapped(HttpServletRequest request, @CurrentlyLoggedUser User user,
														   @PathVariable String serviceName,
														   @PathVariable String resourceName){

		LOGGER.debug("GET request on " +
				request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE));

		ExtendedResourceMapper mapper;
		try {
			mapper = mapperFactory.getResourceMapper(request, serviceName, resourceName);
		} catch (MapperException e) {
			LOGGER.warn(e.getMessage());
			throw new ResourceNotFoundException("Resource not found", request, null);
		}

		ResponseEntity<Resource<Object>> re = restTemplateFactory.responseGET(request, mapper);


		try {
			String resourceClassName = mapper.getMapResourceClassName();
			Class<?> resourceClass = resourceFactory.getResourceClass(resourceClassName);
			databaseMigrator.migrate(re.getBody().getContent(), resourceClass);
		} catch (ClassNotFoundException e) {
			LOGGER.error("Mapper has map for \"" + mapper.getMapResourceClassName() + "\" but class not found.");
			throw new ResourceNotFoundException("Resource not found", request, mapper);
		}

		Object payload = re.getBody();

		ApiResource apiResource = new ApiResource(user.getUserData(), payload);
		Resource<ApiResource> resource = new Resource<>(apiResource);

		resource.add(AffordanceBuilder.linkTo(ApiController.class)
				.slash(mapper.getOriginalPath()).withSelfRel());


		AffordanceContext context;
		try {
			context = contextFactory.getContext(user.getUserData(), re.getBody().getContent());
		} catch (ContextManagerException e) {
			LOGGER.error(e.getMessage());
			throw new InternalServerErrorException("Something went wrong", request, mapper);
		}


		annotator.annotate(resource, context);

		return ResponseEntity.status(re.getStatusCode()).body(resource);
	}

	@ExceptionHandler(RestTemplateFactoryException.class)
	public ResponseEntity<Resource<ApiResource>> handleRestTemplateFactoryException(
			RestTemplateFactoryException ex, @CurrentlyLoggedUser User user){

		// TODO determine headers
		ApiResource apiResource = new ApiResource(user.getUserData(), ex.getResponseData());
		Resource<ApiResource> resource = new Resource<>(apiResource);
		return ResponseEntity.status(ex.getHttpStatus()).body(resource);
	}

	@ExceptionHandler(ServiceUnavailableException.class)
	public ResponseEntity<ErrorResource> handleServiceUnavailableException(ServiceUnavailableException ex){
		ErrorResource errorResource = new ErrorResource();

		errorResource.setPath(ex.getPath());
		String serviceName = "service";
		if (ex.getMapper() != null){
			serviceName = ex.getMapper().getServiceName();
		}
		errorResource.setError(serviceName + " did not respond.");
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(errorResource);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<ErrorResource> handleResourceNotFoundException(ResourceNotFoundException ex){
		ErrorResource errorResource = new ErrorResource();

		errorResource.setPath(ex.getPath());
		errorResource.setError(ex.getSpecificErrorMessage());

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResource);
	}

	@ExceptionHandler(InternalServerErrorException.class)
	public ResponseEntity<ErrorResource> handleInternalServerErrorException(InternalServerErrorException ex){
		ErrorResource errorResource = new ErrorResource();

		errorResource.setPath(ex.getPath());
		errorResource.setError(ex.getSpecificErrorMessage());

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResource);
	}

}
