package gr.ntua.softeng.affordances.proxy.annotator.context;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.builder.DefaultContextBuilder;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.utils.SystemData;

import javax.annotation.Nonnull;

public class ContextBuilderImpl extends DefaultContextBuilder {

    private static String resourceNameUserData = UserData.class.getName();
    private static String resourceNameSystemData = SystemData.class.getName();

    public ContextBuilderImpl(){
    }


    @Override
    @Nonnull
    public AffordanceContext build() {

        ContextKey ageKey = new ContextKey(resourceNameUserData, "age");
        ContextKey monthKey = new ContextKey(resourceNameSystemData, "month");
        ContextKey nameKey = new ContextKey(resourceNameUserData, "name");
        ContextKey  seasonKey = new ContextKey(resourceNameSystemData, "season");

        super.addConjunction(ageKey, seasonKey).addConjunction(nameKey, monthKey);
        return super.build();

    }
}
