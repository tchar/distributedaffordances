package gr.ntua.softeng.affordances.proxy.resource;

public class ErrorResource {
    private String error;
    private String path;

    public ErrorResource(){

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
