package gr.ntua.softeng.affordances.proxy.repository.user;

import gr.ntua.softeng.affordances.proxy.repository.ExtendedRepository;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.affordances.proxy.repository.entities.User;
import gr.ntua.softeng.affordances.proxy.repository.entities.UserData;
import gr.ntua.softeng.affordances.proxy.repository.userdata.UserDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class UserRepositoryImpl implements ExtendedRepository<User> {
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserDataRepository userDataRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public User saveOrUpdate(User user) throws EntityValidationException {
		Long userDataIdToDelete = null;
		user = user.validated();
		User foundUser = userRepository.findUserByUsername(user.getUsername());
		if (foundUser != null){
			user.setId(foundUser.getId());
			if (!foundUser.getUserData().getId().equals(user.getUserData().getId())){
				userDataIdToDelete = foundUser.getUserData().getId();
			}
		}
		if (user.getUserData() == null){
			user.setUserData(new UserData());
		}
		userDataRepository.save(user.getUserData());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		User retUser = userRepository.save(user);
		if (userDataIdToDelete != null){
			userDataRepository.delete(userDataIdToDelete);
		}
		return retUser;
	}
}
