package gr.ntua.softeng.affordances.proxy.repository.entities;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.persistence.*;

import gr.ntua.softeng.affordances.proxy.SpringContextBridge;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.gm.DecompositionType;
import gr.ntua.softeng.affordances.framework.evaluator.compare.DefaultOperation;
import gr.ntua.softeng.affordances.proxy.annotator.context.ContextFactory;
import gr.ntua.softeng.affordances.proxy.repository.EntityValidator;
import gr.ntua.softeng.affordances.proxy.resource.ResourceFactory;

@Entity
@Table(name="GOAL_MODEL_NODES")
public class GoalModelNode implements EntityValidator<GoalModelNode>{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	// Common fields
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private NodeType type;
	
	// Node fields
	private DecompositionType decType;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private List<GoalModelNode> children;
	
	// Leaf fields
	
	private String resourceType;
	private String resourceField;
	private OP opType;
	private String value;
	private Double weight;
	
	public GoalModelNode(){
		
	}
	
	private void validate() throws EntityValidationException {
		if (name == null)
			throw new EntityValidationException("Node name cannot be empty");
		if (type == null)
			throw new EntityValidationException("\"" + name + "\": type cannot be empty");
		
    	String s = "^[a-zA-Z0-9_]+$";
				
		if (!Pattern.compile(s).matcher(name).find())
			throw new EntityValidationException("Invalid node name \"" + name +"\". Example node_1");
		
		if (type.equals(NodeType.NODE)){
			if (decType == null)
				throw new EntityValidationException("\"" + name + "\": decomposition type cannot be empty");
			if (children == null || this.children.size() < 2)
				throw new EntityValidationException("\"" + name + "\": must have at least 2 children");
			
			for (GoalModelNode node: children){
				node.validated();
			}
		} else if (type.equals(NodeType.LEAF)){
			if (resourceType == null)
				throw new EntityValidationException("\"" + name + "\": resource type cannot be empty");
			if (resourceField == null)
				throw new EntityValidationException("\"" + name + "\": resource field cannot be empty");
			if (opType == null)
				throw new EntityValidationException("\"" + name + "\": OP cannot be empty");
			if (value == null)
				throw new EntityValidationException("\"" + name + "\": value cannot be empty");
			if (weight == null)
				throw new EntityValidationException("\"" + name + "\": weight cannot be empty");

			Set<String> resourceClassNames = SpringContextBridge.services().resourceFactory().getResourceClassNames();
			Map<String, Map<String, List<String>>> contextMap = SpringContextBridge
													.services().contextFactory().getContextMap();
			if (!resourceClassNames.contains(resourceType))
				throw new EntityValidationException("Resource \"" + resourceType + "\" not found");
			
			if(!contextMap.containsKey(resourceType))
				throw new EntityValidationException("Resource \"" + resourceType +
													"\" does not exist in context map.");
			
			if (!contextMap.get(resourceType).keySet().contains(resourceField))
				throw new EntityValidationException("Resource field \"" + resourceField +
						"\" does not exist in context map with key \"" + resourceType + "\".");

			if (!contextMap.get(resourceType).get(resourceField).contains(opType.toString()))
				throw new EntityValidationException("Resource field \"" + resourceField +
						"\" does not suport opType \"" + opType + "\".");
			
			if (weight < 0 || weight > 100)
				throw new EntityValidationException("\"" + name + "\": weight must be between 0 and 100");
			
		} else {
			throw new EntityValidationException("\"" + name + "\": must be either leaf or node");
		}
	}

	@Transient
	@Override
	public boolean isValid() {
		try {
			validate();
		} catch (EntityValidationException e){
			return false;
		}
		return true;
	}
	
	@Override
	public GoalModelNode validated() throws EntityValidationException {
		validate();
		return this;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public NodeType getType() {
		return type;
	}

	public void setType(NodeType type) {
		this.type = type;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceField() {
		return resourceField;
	}

	public void setResourceField(String resourceField) {
		this.resourceField = resourceField;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public DecompositionType getDecType() {
		return decType;
	}

	public void setDecType(DecompositionType decType) {
		this.decType = decType;
	}

	public List<GoalModelNode> getChildren() {
		return children;
	}

	public void setChildren(List<GoalModelNode> children) {
		this.children = children;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public OP getOpType() {
		return opType;
	}

	public void setOpType(OP opType) {
		this.opType = opType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public enum NodeType {
		NODE, LEAF
	}
	
	public enum OP {
		GT, GE, LT, LE, EQ;


		public DefaultOperation.Type toDefaultOperationType(){
			if (this.equals(OP.EQ))
				return DefaultOperation.Type.EQ;
			if(this.equals(OP.LT))
				return DefaultOperation.Type.LT;
			if(this.equals(OP.LE))
				return DefaultOperation.Type.LE;
			if(this.equals(OP.GT))
				return DefaultOperation.Type.GT;
			if(this.equals(OP.GE))
				return DefaultOperation.Type.GE;
			return DefaultOperation.Type.EQ;
		}

	}	
}
