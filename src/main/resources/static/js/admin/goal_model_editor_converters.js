function convertModelToRemote(model){

	function getRoots(m){
		var roots = [];
		m.nodes.forEach(function(v, i){
			if (v.parent == undefined) return;
			if (v == v.parent) roots.push(v);
		});
		return roots;
	};

	function hasDuplicateNames(nodes){
		var names = {};
		nodes.forEach(function(v, i){
			names[v.name] = v.name;
		});
		return Object.keys(names).length != nodes.length;
	};

	function fromLocalNode(node){
		if (node.type == 'NODE'){
			return {
				name:node.name,
				type: 'NODE',
				decType: node.decType
			}

		} else if (node.type == 'LEAF'){
			return {
				name: node.name,
				type: 'LEAF',
				resourceType: node.resourceType,
				resourceField: node.resourceField,
				opType: node.opType,
				value: node.value,
				weight: node.weight
			}
		}
	};

	function getChildren(remoteNode, nodes){
		var children = [];
		nodes.forEach(function(v, i){
			if (v.parent == undefined) return;
			if (remoteNode.name == v.parent.name &&
								v.name != v.parent.name){ 
				children.push(fromLocalNode(v));
			}
		});
		return children;
	}

	function rec(node, nodes){
		var children = getChildren(node, nodes);
		var index = children.indexOf(node);
		if (index > -1) children.splice(index, 1);
		children.forEach(function(v, i){
			v.children = rec(v, nodes);
		});
		return children;
	};

	if (model.name == undefined || 
				model.name.trim() == ''){
		alert('No goal model name specified');
		return;
	}

	if (model.affordance == undefined || 
				model.affordance.trim() == ''){
		alert('No goal model affordance specified');
		return;
	}

	var remoteModel = { name: model.name,
					    affordance: model.affordance};

	var baseRes = model.baseResource;

	if (baseRes != undefined && !baseRes['None']){
		remoteModel.baseResourceClassName = baseRes;
	}

	var roots = getRoots(model);
	if (roots.length != 1){
		alert('Only one root is permitted');
		return;
	}
	remoteModel.root = fromLocalNode(roots[0]);

	if (hasDuplicateNames(model.nodes)){
		alert("Nodes with the same name found.");
		return;
	}

	remoteModel.root.children = rec(remoteModel.root, model.nodes);
	return remoteModel;
};

function convertModelToLocal(model){

	function fromRemoteNode(node){
		return {
			name: node.name,
			type: node.type,
			decType: node.decType,
			resourceType: node.resourceType,
			resourceField: node.resourceField,
			opType: node.opType,
			value: node.value,
			weight: node.weight
		};
	};


	function getNodes(m){
		var nodes = [];
		var parents = {};
		var stack = [m.root];
		while (stack.length != 0){
			var currentNode = stack.shift();
			var newLocalNode = fromRemoteNode(currentNode);
			if (currentNode.children != undefined){
				stack = currentNode.children.concat(stack);
				currentNode.children.forEach(function(v, i){
					parents[v.name] = newLocalNode;
				});
			}
			if (parents[newLocalNode.name] == undefined)
				newLocalNode.parent = newLocalNode;
			else
				newLocalNode.parent = parents[newLocalNode.name];
			nodes.push(newLocalNode);
		}
		return nodes;
	};

	var localModel = {name: model.name,
					  affordance: model.affordance};

	var baseRes = model.baseResourceClassName;
	if (baseRes == undefined){
		baseRes = 'None';
	}

	localModel.baseResource = baseRes;

	localModel.nodes = getNodes(model);
	return localModel;
};

function convertModelsToLocal(models){
	if (models == undefined) return [];
	var localModels = [];
	models.forEach(function(v, i){
		localModels.push(convertModelToLocal(v));
	});
	return localModels;
};

function cloneLocalModel(model){
	var clone = {	name: model.name,
				 	affordance: model.affordance,
				 	baseResource: model.baseResource,
				 	nodes: [],
				 	nodeParents: []
	};

	var nodes = {};

	model.nodes.forEach(function(v, i){
		var node = {
			name: v.name,
			type: v.type,
			decType: v.decType,
			resourceType: v.resourceType,
			resourceField: v.resourceField,
			opType: v.opType,
			value: v.value,
			weight: v.weight
		};

		nodes[node.name] = node;
		clone.nodes.push(node);
	});

	model.nodes.forEach(function(v, i){
		nodes[v.name].parent = nodes[v.parent.name];
	});

	clone.nodes.forEach(function(v, i){
		if (v.type == 'NODE')
			clone.nodeParents.push(v);
	});

	return clone;
};