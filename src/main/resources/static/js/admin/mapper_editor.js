var baseUrl = "/admin/mapper";

function ajax(http, url, method, data, successCall, errorCall){
	http({method: method, url: url, data: data})
	.then(function success(data){
		console.log('Success');
		successCall(data);
   }, function error(data){
		console.log('Error');
		errorCall(data);
	});
};

function cloneMap(map){
	var newMap = {
		originalPath: map.originalPath,
		forwardBaseAddress: map.forwardBaseAddress,
		forwardPath: map.forwardPath,
		resourceClassName: map.resourceClassName,
		httpMethod: map.httpMethod,
		httpHeaders: []
	}

	if (map.httpHeaders != undefined){
		var headerId = 0;
		map.httpHeaders.forEach(function(v, i){
			var newHeader = {id: headerId++, 
							 name: v.name, value: v.value}
			newMap.httpHeaders.push(newHeader);
		});
	}
	return newMap;
}

function simplifyResourceName(resource){
	if (resource == undefined) return;
	var list = resource.split('.');
	var simpleName = list.splice(list.length - 1, 1)[0];
	list = list.map(function(elem){
		return elem.charAt(0);
	});
	list.push(simpleName);
	return list.join('.');
}

var app = angular.module('mapperEditor', ['ngMaterial', 'ngMessages', 
										  'material.svgAssetsCache']);

app.config(['$httpProvider', function($httpProvider) {
	var token = 'a29zdGFzOmtvc3Rhcw==';
    // $httpProvider.defaults.headers.common['Authorization'] = 'Basic ' + token;
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
    // $httpProvider.defaults.withCredentials = true;
}]);

app.config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider
      .iconSet('icons', '/img/admin/icons.svg', 24)
      .defaultIconSet('img/icons/sets/core-icons.svg', 24);
}]);

app.controller('parentController', function($http, $mdSidenav, $timeout, $log){
	var self = this;
	self.currentMap = {httpHeaders: [], httpMethod: 'GET'};
	self.status = {result: '', message: ''};
	self.ajaxLoading = false;
	self.virtualLoading = false;
	self.isLoading = false;

    self.openLeft = function(){
    	$mdSidenav('left').open();
    }

    self.closeLeft = function(){
    	$mdSidenav('left').close();
    }

	self.setData = function(data){
		var newData = data.data.data
		self.maps = newData.maps;
		if (self.maps != undefined){
			self.maps.sort(function(a, b){
				return a.originalPath > b.originalPath;
			});
		}
		self.resources = newData.resources;
	}

	self.success = function(data){
		self.ajaxLoading = false;
		self.isLoading = self.virtualLoading;
		self.status = {result: 'Success', message: data.data.message};
		self.setData(data);
	}

	self.error = function(data){
		self.ajaxLoading = false;
		self.isLoading = self.virtualLoading;
		var message = 'Something went wrong.';
		if (data != undefined && data.data != undefined &&
							data.data.message != undefined){
			message = data.data.message;
		}
		self.status = {result: 'Error', message: message};
	}

	self.createMap = function(){
		self.ajaxLoading = true;
		virtualTimeout();
		ajax($http, baseUrl, 'POST', 
				self.currentMap, self.success, self.error);
	}

	self.deleteMap = function(map){
		self.ajaxLoading = true;
		virtualTimeout();
 		ajax($http, baseUrl, 'DELETE', 
 			map, self.success, self.error);
	}

	self.setCurrentMap = function(index){
		self.currentMap = cloneMap(self.maps[index]);
		self.closeLeft();
	}

	var virtualTimeout = function(){
		self.isLoading = true;
		self.virtualLoading = true;
		$timeout(() => {
			self.virtualLoading = false;
			self.isLoading = self.ajaxLoading;
		}, 1000);
	}

	ajax($http, baseUrl + '/getAll', 'GET', 
			undefined, self.setData, self.error);
});

app.component('mapsContainer', {
	bindings: {
		maps: '<',
		setCurrentMap: '<'
	},
	
	templateUrl: '/templates/admin/template_maps_sidenav.html'
});

app.component('mapForm', {
	bindings : {
		currentMap: '=',
		resources: '<',
		createMap: '<',
		deleteMap: '<',
		isLoading: '<'
	},

	templateUrl: '/templates/admin/template_maps_form.html',

	controller: function($log){
		var self = this;

		self.headerId = 0;

		self.httpMethods = ['GET', 'POST', 'DELETE'];

		self.simplifyResourceName = simplifyResourceName;

		self.addHeader = function (){
			self.currentMap.httpHeaders.push({id: self.headerId++, 
											  name: '', value: ''});
		}

		self.deleteHeader = function(index){
			self.currentMap.httpHeaders.splice(index, 1);
		}
	}
});

app.component('statusBar', {
	bindings: {
		status: '<',
		isLoading: '<'
	},

	templateUrl: '/templates/admin/template_status_bar.html'
});


app.controller('headersController', function($log){
    var self = this;

    self.headerNameSearch = headerNameSearch;
    self.headerValueSearch = headerValueSearch;

    //filter function for search query
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(header) {
            return (header.toLowerCase()
                        .indexOf(lowercaseQuery) === 0);
        }
    }

    function headerNameSearch(header){
        var values = Object.keys(commonHeaders);
        var results = header.name ? values.filter(
            createFilterFor(header.name) ) : values, deferred;
        return results;
    }

    function headerValueSearch(header){
        var values = commonHeaders[header.name]
        if (values === undefined) values = [];
        var results = header.value ? values.filter(
            createFilterFor(header.value) ) : values, deferred;
        return results;
    }

});


var commonHeaders = {
    'Accept' : ['application/xml', 'application/json', 
                'application/ld+json', 'application/hal+json'],
    'Accept-Charset': ['utf-8', 'ISO-8859-1'],
    'Accept-Encoding': ['compress', 'deflate', 'exi', 'gzip', 
                        'identity', 'pack200-gzip', 'br', 'bzip2', 
                        'lzma', 'peerdist', 'sdch', 'xpress', 'xz'], 
    'Accept-Language': ['en-US'], 
    'Accept-Datetime': ['Thu, 31 May 2007 20:35:00 GMT'], 
    'Authorization': ['Basic ', 'Bearer '],   
    'Cache-Control': ['no-cache',  'max-age=<seconds>',
                      'max-stale=<seconds>', 'min-fresh=<seconds>',
                      'no-cache', 'no-store',
                      'no-transform', 'only-if-cached'],
    'Connection': ['keep-alive', 'close'],
    'Cookie': ['name=value; name2=value2; name3=value3'], 
    'Content-Length': [],  
    'Content-MD5': [], 
    'Content-Type': ['application/xml', 'application/json', 
                     'application/ld+json', 'application/hal+json'],    
    'Date': ['Thu, 31 May 2007 20:35:00 GMT'],    
    'Expect': [],  
    'Forwarded': [],   
    'From': ['webmaster@example.org'],    
    'Host': [],    
    'If-Match': [],    
    'If-Modified-Since': [],   
    'If-None-Match': [],   
    'If-Range': [],   
    'If-Unmodified-Since': [], 
    'Max-Forwards': [],   
    'Origin': [],
    'Proxy-Authorization': ['Basic ', 'Bearer '], 
    'Range': [],   
    'Referer': [], 
    'TE': ['compress', 'deflate', 'gzip', 'trailers'],  
    'User-Agent': ['Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',
                    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
                    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41',
                    'Mozilla/5.0 (Linux; U; Android 4.0.3; de-ch; HTC Sensation Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30',
                    'Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)',
                    'Googlebot/2.1 (+http://www.google.com/bot.html)'],  
    'Upgrade': [], 
    'Via': [],
}