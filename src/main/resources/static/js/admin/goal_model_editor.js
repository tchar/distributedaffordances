var baseUrl = "/admin/goalmodel";

var app = angular.module('goalModelEditor', ['ngMaterial', 'ngMessages', 
										  	 'material.svgAssetsCache']);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
}]);

app.config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider
      .iconSet('icons', '/img/admin/icons.svg', 24)
      .defaultIconSet('img/icons/sets/core-icons.svg', 24);
}]);

app.controller('parentController', function($http, $mdSidenav, $timeout, $log){
	var self = this;

	self.models = [];
	self.contexts = {};
	self.externalContextKeys = {'None': true};
	self.currentModel = {name: '', affordance: '', 
					   baseResource: 'None', nodes: [], 
					   nodeParents: []};
	self.ajaxLoading = false;
	self.virtualLoading = false;
	self.isLoading = false;
	self.status = {result: '', message: ''};

    self.openLeft = function(){
    	$mdSidenav('left').open();
    }

    self.closeLeft = function(){
    	$mdSidenav('left').close();
    }

	self.setData = function(data){
		var data = data.data.data;
		self.models = convertModelsToLocal(data.goalModels);
		self.models.sort(function(a, b){
			return a.name > b.name;
		});
		self.contexts = data.contextMap;
		data.externalContextKeys.forEach(function(v, i){
			self.externalContextKeys[v] = true;
		});
	};

	self.success = function(data){
		self.ajaxLoading = false;
		self.isLoading = self.virtualLoading;
		self.status = {result: 'Success', message: data.data.message};
		self.setData(data);
	};

	self.error = function(data){
		self.ajaxLoading = false;
		self.isLoading = self.virtualLoading;
		var message = 'Something went wrong.';
		if (data != undefined && data.data != undefined &&
							data.data.message != undefined){
			message = data.data.message;
		}
		self.status = {result: 'Error', message: message};
	}

	self.deleteModel = function(model){
		if (model.name != undefined && model.name.trim() != ''){
			self.ajaxLoading = true;
			virtualTimeout();
			ajax($http, baseUrl + '/' + model.name, 'DELETE', 
								undefined, self.success, self.error);
		}
	};

	self.createModel = function(model){
		model = convertModelToRemote(model);
		if (model == undefined) return;
		self.ajaxLoading = true;
		virtualTimeout();
		ajax($http, baseUrl, 'POST', model, self.success, self.error);
	};

	self.setCurrentModel = function(model){
		self.currentModel = cloneLocalModel(model);
		self.closeLeft();
	}

	var virtualTimeout = function(){
		self.isLoading = true;
		self.virtualLoading = true;
		$timeout(() => {
			self.virtualLoading = false;
			self.isLoading = self.ajaxLoading;
		}, 1000);
	}

	ajax($http, baseUrl + '/getAll', 'GET', undefined, self.setData, self.error);
});

app.component('modelsContainer', {
	bindings: {
		models: '<',
		setCurrentModel: '<'
	},

	templateUrl: '/templates/admin/template_models_sidenav.html'
});

app.component('modelForm', {
	bindings: {
		currentModel: '<',
		resources: '<',
		externalResourceKeys: '<',
		createModel: '<',
		deleteModel: '<'
	},

	templateUrl: '/templates/admin/template_models_form.html',

	controller: 'currentModelCtrl'
});

app.controller('currentModelCtrl', function($log){
	var self = this;

	self.nodeTypes = [{name: 'NODE', value: 'Node'},
					{name: 'LEAF', value: 'Leaf'}];

	self.decTypes = [{name: 'AND', value: 'and'}, 
				   {name: 'OR', value: 'or'}];

	self.opTypes = [{name: 'EQ', value: '='},
				  {name: 'GT', value: '>'},
				  {name: 'GE', value: '>='},
				  {name: 'LT', value: '<'},
				  {name: 'LE', value: '<='}];

	self.simplifyResourceName = simplifyResourceName;

	self.filteredResources = function(){
		var resourceKeys = Object.keys(self.resources);
		var base = self.currentModel.baseResource;
		var external = self.externalResourceKeys;
		var tmp = {};

		resourceKeys.forEach(function(v, i){
			if (!external[v] || v == base){
				tmp[v] = self.resources[v];
			}
		});
		return tmp; 
	} 

	self.addNode = function(){
		var nodeParent = undefined;
		var parents = self.getParents();
		if (parents.length != 0){
			nodeParent = parents[0];
		}

		var node = {
			type: 'NODE', parent: nodeParent, decType: 'AND',
			opType: 'EQ', weight: '100'
		};
		self.currentModel.nodes.push(node);
	};

	self.deleteNode = function(index){
		self.currentModel.nodes.splice(index, 1);
		validateModelStructure(self.newModelForm);
	}

	self.clearModel = function(){
		self.currentModel = { name: '', affordance: '', 
						   	baseResource: 'None', 
						   	nodes: [], nodeParents: []};
		self.addNode();
		self.resetForm();
	}

	self.resetForm = function(){
		self.newModelForm.$setPristine();
		self.newModelForm.$setUntouched();
		validateNodeNames(self.newModelForm);
		validateModelStructure(self.newModelForm);
	}

	self.getParents = function(){
		return self.currentModel.nodes.filter(function(v){
			return (v.type == 'NODE' && v.name != undefined);
		});
	}

	self.resourceFields = function(node){
		if (node.resourceType == undefined) return [];
		var t = self.resources[node.resourceType];
		if (t == undefined) return [];
		return Object.keys(t);
	}

	self.filteredOpTypes = function(node){
		if (node.resourceType == undefined) return [];
		if (node.resourceField == undefined) return [];
		var t = self.resources[node.resourceType];
		if (t == undefined) return [];
		var ops = t[node.resourceField];
		if (ops == undefined) return [];

		return self.opTypes.filter(function(v){
			return (ops.indexOf(v.name) != -1);
		}); 
	}

	self.$onInit = function() {
		self.addNode();
	}

	self.$onChanges = function(val){
		if (val.currentModel != undefined &&
				!val.currentModel.isFirstChange()){
			self.resetForm();
		}	
	}
});

app.directive('uniqueName', function ($log){ 
	return {
		restrict: 'A',
		require: 'ngModel',
		scope : {
			modelForm: '<uniqueName'
		},
		link: function(scope, elem, attr, ngModel) {
	  		var modelForm = scope.modelForm;
			scope.$on('$destroy', function() {
        		validateNodeNames(modelForm);
				validateModelStructure(modelForm);
      		});
		  	ngModel.$parsers.unshift(function (value) {
		  		validateNodeNames(modelForm);
				validateModelStructure(modelForm);
		  		return value;
		  	});
		}
   	};
});

app.directive('modelStructure', function($log){
	return {
		restrict: 'A',
		require: 'ngModel',
		scope : {
			modelForm: '<modelStructure'
		},
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				var modelForm = scope.modelForm;
				validateModelStructure(modelForm);
				return value;
			});
		}
	};
});

app.component('statusBar', {
	bindings: {
		status: '<',
		isLoading: '<'
	},

	templateUrl: '/templates/admin/template_status_bar.html'
});

function ajax($http, url, method, data, successCall, errorCall){
	$http({method: method, url: url, data: data})
	.then(function success(data){
		console.log('Success');
		successCall(data);
   }, function error(data){
		console.log('Error');
		errorCall(data);
	});
}

function validateModelStructure(modelForm){
	var i = 0;
	var children = {};
	var roots = {};
	var leaves = {};
	var rootCnt = 0;


	while (modelForm['nodeParent' + i] != undefined){
		var curNodeNameModel = modelForm['nodesName' + i];
		var curNodeTypeModel = modelForm['nodeType' + i];
		var curParentModel = modelForm['nodeParent' + i++];

		var nodeName = curNodeNameModel.$viewValue;
		var nodeType = curNodeTypeModel.$viewValue;
		if (nodeType == 'LEAF'){
			leaves[nodeName] = true;
		}

		var parentName = curParentModel.$viewValue;
		if (parentName == undefined) continue;
		parentName = parentName.name;
		if (nodeName == parentName && nodeType != 'LEAF') {
			rootCnt++;
			roots[nodeName] = true;
			continue;
		}
		if (children[parentName] == undefined){
			children[parentName] = 1;
		} else {
			children[parentName]++;
		}
	}
	
	i = 0;
	
	while (modelForm['nodesName' + i] != undefined){
		var curNodeNameModel = modelForm['nodesName' + i];
		var curNodeTypeModel = modelForm['nodeType' + i];
		var curParentModel = modelForm['nodeParent' + i++];
		
		var nodeName = curNodeNameModel.$viewValue;
		var nodeType = curNodeTypeModel.$viewValue;
		var nodeChildren = children[nodeName];


		var validRoot = ((rootCnt == 1) ||
						 (roots[nodeName] == undefined && 
						  rootCnt > 1) || (nodeType == 'LEAF'));
		curNodeNameModel.$setValidity('duplicateRoot', validRoot);
		
		var validStruct = (nodeChildren != undefined &&
						  nodeChildren > 1 ||
						  nodeType == 'LEAF');
		curNodeNameModel.$setValidity('modelStructure', validStruct);
		
		var nodeParent = curParentModel.$viewValue;
		if (nodeParent == undefined) continue;
		nodeParent = nodeParent.name;
		if (leaves[nodeParent]){
			curParentModel.$setViewValue('');
			curParentModel.$setPristine();
			curParentModel.$render();
		}
	}
}

function validateNodeNames(modelForm){
	var names = {};
	var i = 0;
	var untouched = false;

	while (modelForm['nodesName' + i]){
		var curNodeNameModel = modelForm['nodesName' + i++];
		var name = curNodeNameModel.$viewValue;
		untouched = untouched || curNodeNameModel.$untouched;
		if (names[name] != undefined){
			names[name]++;
		} else {
			names[name] = 1;
		}
	}

	i = 0;
	while (modelForm['nodesName' + i]){
		var curNodeNameModel = modelForm['nodesName' + i];
		var curParentModel = modelForm['nodeParent' + i++];
		var viewVal = curNodeNameModel.$viewValue;
		var isValid = (names[viewVal] == 1);
		curNodeNameModel.$setValidity('uniqueName', isValid);
	}
}

function simplifyResourceName(resource){
	if (resource == undefined) return;
	var list = resource.split('.');
	var simpleName = list.splice(list.length - 1, 1)[0];
	list = list.map(function(elem){
		return elem.charAt(0);
	});
	list.push(simpleName);
	return list.join('.');
}