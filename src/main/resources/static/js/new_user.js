function ajax(http, url, method, data, successCall, errorCall){
	http({method: method, url: url, data: data})
	.then(function success(data){
		console.log('Success');
		successCall(data);
   }, function error(data){
		console.log('Error');
		errorCall(data);
	});
};

var app = angular.module('newUser', ['ngMaterial', 'ngMessages', 
										  'material.svgAssetsCache']);

app.config(['$httpProvider', function($httpProvider) {
	$httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
}]);

app.controller('parentController', function($http, $timeout, $log){
	var self = this;

	self.status = {result: '', message: ''};
	self.ajaxLoading = false;
	self.virtualLoading = false;
	self.isLoading = false;

	self.newUser = {interests: []};


	self.success = function(data){
		$log.debug('Success');
		self.ajaxLoading = false;
		self.isLoading = self.virtualLoading;
		self.status = {result: 'Success', message: data.data.message};
	}

	self.error = function(data){
		$log.debug('Error');
		self.ajaxLoading = false;
		self.isLoading = self.virtualLoading;
		var message = 'Something went wrong.';
		if (data != undefined && data.data != undefined &&
							data.data.message != undefined){
			message = data.data.message;
		}
		self.status = {result: 'Error', message: message};
	}

	self.createUser = function(){
		self.ajaxLoading = true;
		virtualTimeout();
		ajax($http, "/user", 'POST', 
				self.newUser, self.success, self.error);
	}

	var virtualTimeout = function(){
		self.isLoading = true;
		self.virtualLoading = true;
		$timeout(() => {
			self.virtualLoading = false;
			self.isLoading = self.ajaxLoading;
		}, 1000);
	}

});

app.component('userForm',{
	bindings: {
		newUser: '=',
		createUser: '<'
	}, 

	templateUrl: '/templates/template_user_form.html',

	controller: function(){
		var self = this;

		self.maxDate = new Date();

		self.formSubmit = function(){
			self.newUser.birthDate = self.birthDate.getTime();
			self.createUser();
		}
	}
})

app.component('statusBar', {
	bindings: {
		status: '<',
		isLoading: '<'
	},

	templateUrl: '/templates/admin/template_status_bar.html'
});