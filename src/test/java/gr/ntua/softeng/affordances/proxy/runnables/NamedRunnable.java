package gr.ntua.softeng.affordances.proxy.runnables;

public abstract class NamedRunnable implements Runnable{

	protected String name;
		
	public NamedRunnable(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object o) {
		return o != null &&
				o instanceof NamedRunnable &&
				this.name.equals(((NamedRunnable) o).getName());
	}
	
	@Override
	public int hashCode(){
		return this.name.hashCode();
	}
}
