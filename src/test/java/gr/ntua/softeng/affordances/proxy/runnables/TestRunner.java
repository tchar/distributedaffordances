package gr.ntua.softeng.affordances.proxy.runnables;

import gr.ntua.softeng.affordances.proxy.TestWrapper;

public class TestRunner extends NamedRunnable {
	
	private TestWrapper test;
	
	public TestRunner(String name, TestWrapper test){
		super(name);
		this.test = test;
	}
	
	@Override
	public void run() {  
		System.out.println(this.name + " started");
		test.run();
		System.out.println(this.name + " finished normally.");
	}
}
