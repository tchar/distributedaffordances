package gr.ntua.softeng.affordances.proxy.client;

import gr.ntua.softeng.affordances.proxy.AsyncTester;
import gr.ntua.softeng.affordances.proxy.restcalls.RestTemplateTest;
import org.junit.Test;

public class RestTemplateTests {

	private static final long TIME_TO_JOIN = 10000;
	
	@Test
	public void normalRestCallTest(){
		testJson("0").run();
		testHalJson("0").run();
		// This test fails
//		testJsonLd("0").run();
	}
	
	@Test
	public void asyncRestCallTest(){
		AsyncTester at = new AsyncTester();
		int j = 1;
		
		for (int i = 0; i < 10; i ++){
			String id = Integer.toString(i+j);
			at.add(id, testJson(id));
		}
		
		at.startAll();
		at.testAll(TIME_TO_JOIN);

		for (int i = 0; i < 10; i ++){
			String id = Integer.toString(i+j);
			at.add(id, testHalJson(id));
		}
		
		at.startAll();
		at.testAll(TIME_TO_JOIN);

		// This test fails
//		for (int i = 0; i < 10; i ++){
//			String id = Integer.toString(i+j);
//			at.add(id, testJsonLd(id));
//		}
//		
//		at.startAll();
//		at.testAll(TIME_TO_JOIN);
	}
	
	private RestTemplateTest testJson(String id){
		RestTemplateTest test = new RestTemplateTest(id);
		test.setUrl("http://localhost:9420/api/order/1");
		test.setContentType(RestTemplateTest.MEDIATYPE_JSON);
		test.addHttpMessageConverter(RestTemplateTest.jsonConverter());
		return test;
	}
	
	private RestTemplateTest testHalJson(String id){
		RestTemplateTest test = new RestTemplateTest(id);
		test.setUrl("http://localhost:9420/api/order/1");
		test.setContentType(RestTemplateTest.MEDIATYPE_HALJSON);
		test.addHttpMessageConverter(RestTemplateTest.halConverter());
		return test;
	}
	
	@SuppressWarnings("unused")
	private RestTemplateTest testJsonLd(String id){
		RestTemplateTest test = new RestTemplateTest(id);
		test.setUrl("http://localhost:9420/api/order/1");
		test.setContentType(RestTemplateTest.MEDIATYPE_JSONLD);
		test.addHttpMessageConverter(RestTemplateTest.hydraConverter());
		return test;
	}
	
}
