package gr.ntua.softeng.affordances.proxy;

public interface TestWrapper {

	void run();
	
}
