package gr.ntua.softeng.affordances.proxy.restcalls;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import gr.ntua.softeng.affordances.proxy.TestWrapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.escalon.hypermedia.spring.hydra.HydraMessageConverter;
import gr.ntua.softeng.affordances.proxy.resource.ApiResource;

public class RestTemplateTest implements TestWrapper {
	
	private String id;

	private String url;
	private MediaType contentType;
	private List<MediaType> accepts;
	private List<HttpMessageConverter<?>> converters;
	
	public static final MediaType MEDIATYPE_JSON = MediaType.parseMediaType("application/json");
	public static final MediaType MEDIATYPE_JSONLD = MediaType.parseMediaType("application/ld+json");
	public static final MediaType MEDIATYPE_HALJSON = MediaType.parseMediaType("application/hal+json");
	
	public RestTemplateTest(String id){
		this.id = id;
		this.accepts = new LinkedList<>();
		this.converters = new LinkedList<>();
	}
	
	private void print(Object obj){
		System.out.println(this.id + ": " + obj);
	}
	
	public void run(){
		RestTemplate restTemplate;
		boolean rWasEmpty = false;
		boolean cWasEmpty = true;
		boolean aWasEmpty = true;
		
		if (url == null)
			throw new AssertionError("Url is null");
		if (converters.isEmpty()){
			restTemplate = new RestTemplate();
			rWasEmpty = true;
		} else {
			restTemplate = new RestTemplate(converters);
		}
	
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Auth-Username", "kostas");
		headers.set("X-Auth-Password", "kostas");
		if (contentType != null){
			headers.setContentType(contentType);
			cWasEmpty = false;
		}
		if (!accepts.isEmpty()){
			headers.setAccept(accepts);
			aWasEmpty = false;
		}
		
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		
		System.out.println("************************************");
		if (rWasEmpty)
			print("Default RestTemplate");
		System.out.println("url: " + url);
		if (aWasEmpty){
			print("Default accepts: " + headers.getAccept().toString());
		} else {
			print("Accepts: " + headers.getAccept().toString());
		}
		if (cWasEmpty){
			print("Default Content-Type: " + headers.getContentType());
		} else {
			print("Content-type: " + headers.getContentType().toString());
		}
		
		try{
			ResponseEntity<Resource<ApiResource>> re;
			
			re = restTemplate.exchange(url ,HttpMethod.GET,
					entity, new ParameterizedTypeReference<Resource<ApiResource>>() {});
			
			print("Response Content-Type " + re.getHeaders().getContentType().toString());
			print(re.getBody());
			
			assertNotNull(re.getBody());
			assertNotNull(re.getBody().getLinks());
			assertTrue(!re.getBody().getLinks().isEmpty());
			print(re.getBody().getLinks().toString());
			
		} catch (RestClientException e){
			fail(e.getMessage());
		}	
	}
	
	public void addAccept(String type){
		accepts.add(MediaType.parseMediaType(type));
	}
	
	public void addAccept(MediaType mediaType){
		accepts.add(mediaType);
	}
	
	public void setContentType(String type){
		contentType = MediaType.parseMediaType(type);
	}
	
	public void setContentType(MediaType mediaType){
		contentType = mediaType;
	}
	
	public void setUrl(String url){
		this.url = url;
	}
	
	public void addHttpMessageConverter(HttpMessageConverter<?> converter){
		converters.add(converter);
	}
	
	public static HydraMessageConverter hydraConverter() {
	    return new HydraMessageConverter();
	}
	
	
	public static MappingJackson2HttpMessageConverter halConverter() {
		
	    ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
	    mapper.registerModule(new Jackson2HalModule());
	    MappingJackson2HttpMessageConverter halConverter = new MappingJackson2HttpMessageConverter();
	    halConverter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
	    halConverter.setObjectMapper(mapper);
	    return halConverter;
	}

	
	public static MappingJackson2HttpMessageConverter jsonConverter() {

	    ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	
	    mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);	
	    MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
	    jacksonConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.valueOf("application/json")));
	    jacksonConverter.setObjectMapper(mapper);
	    return jacksonConverter;
	}
}
