package gr.ntua.softeng.affordances.proxy.client;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BcrypTests {

    @Test
    public void simpleTest(){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String password = "abcd";
        String encoded1 = encoder.encode(password);
        String encoded2 = encoder.encode(password);
        assertFalse(encoded1.equals(encoded2));
        assertTrue(encoder.matches(password, encoded1));
        assertTrue(encoder.matches(password, encoded2));
    }
}
