package gr.ntua.softeng.affordances.proxy;

public class TestUtils {

    public static void print(String name, Object obj){
        System.out.println(name + ": " + obj);
    }
}
