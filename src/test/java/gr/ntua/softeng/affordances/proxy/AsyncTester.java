package gr.ntua.softeng.affordances.proxy;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import gr.ntua.softeng.affordances.proxy.runnables.TestRunner;

public class AsyncTester {
	
	private Map<String, Thread> threads;
	private volatile Map<String, String> errors;
	private volatile Map<String, Boolean> finished;
	
	public AsyncTester(){
		threads = new HashMap<>();
		errors = new HashMap<>();
		finished = new HashMap<>();
	}
	
	public void startAll(){
		for(Thread thread: threads.values()){
			thread.start();
		}
	}
	
	public void start(String name){
		Thread thread = threads.get(name);
		if (thread != null){
			thread.start();
		}
	}
	
	private void addRunnable(String name, TestWrapper test){
	    Thread thread = new Thread(() -> {
            try{
                new TestRunner(name, test).run();
            }catch(AssertionError | Exception e){
                errors.put(name, e.getMessage());
            } finally {
                finished.put(name, true);
            }
        });
	    threads.put(name, thread);
	}
	
	public void add(String name, TestWrapper test){
		addRunnable(name, test);
	}
	
	private void test(String name, Thread thread, long joinInt) {
		try {
			thread.join(joinInt);
		    Boolean fd = finished.get(name);
		    if (fd == null || fd.equals(false))
				fail(name + " thread did not finish");
		    if (errors.get(name) != null)
		    	fail(errors.get(name));
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail(name + " thread was interrupted by another thread");
		}
	}
	
	public void test(String name, long joinInt) throws AssertionError{
		Thread thread = threads.get(name);
		if (thread == null){
			fail("Thread with name \"" + name + "\" was not found");
		}
		test(name, thread, joinInt);
	    
	}
	
	public void testAll(long joinInt) {
		for (String name: threads.keySet()){
			test(name, threads.get(name), joinInt);
		}
	}
}
