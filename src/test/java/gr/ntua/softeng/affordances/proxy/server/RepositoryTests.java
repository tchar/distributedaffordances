package gr.ntua.softeng.affordances.proxy.server;

import static org.junit.Assert.assertEquals;

import gr.ntua.softeng.affordances.proxy.repository.EntityValidationException;
import gr.ntua.softeng.affordances.proxy.repository.entities.User;
import gr.ntua.softeng.affordances.proxy.repository.user.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTests {

	@Autowired
	private UserRepository userRepository;

	@Test
	public void testUserRepository() throws EntityValidationException {
		User user = new User();
		user.setUsername("some_random_username");
		user.setPassword("some_random_password");
		User anotherUser = userRepository.saveOrUpdate(user);
		assertEquals(anotherUser, user);
		anotherUser = userRepository.findUserByUsername(user.getUsername());
		assertEquals(anotherUser, user);
		anotherUser = userRepository.findUserByUsernameAndPassword(
								user.getUsername(), user.getPassword());
		assertEquals(user, anotherUser);
		userRepository.delete(user.getId());
	}
}
