package gr.ntua.softeng.affordances.proxy.client;

import org.junit.Test;
import org.springframework.util.AntPathMatcher;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class AntPathMatcherTests {

    private static final String name = AntPathMatcherTests.class.getSimpleName();


    @Test
    public void antMatcherSimple(){
        AntPathMatcher matcher = new AntPathMatcher();
        String pattern = "/{api}/ebay/order/{id}";
        String path = "/api/ebay/order/1";
        assertTrue(matcher.match(pattern, path));

        Map<String, String> map = matcher.extractUriTemplateVariables(pattern, path);

        for (String key: map.keySet()) {
            if (!key.equals("api") && !key.equals("id")){
                fail("Key must be either api or id");
            }
            if (!map.get(key).equals("api") && !map.get(key).equals("1")){
                fail("Value must be either api or 1");
            }
        }
    }

    @Test
    public void antMatcherUrlParameters(){
        AntPathMatcher matcher = new AntPathMatcher();
        String pattern = "/{service}/order/**";
        pattern = matcher.combine("/{api}", pattern);
        String path = "/api/ebay/order/1?q=ferrari";
        assertTrue(matcher.match(pattern, path));
    }

    @Test
    public void antMatcherUrlTranslate(){
        AntPathMatcher matcher = new AntPathMatcher();
        String pattern = "/{service:[a-zA-Z0-9_-]+}/{resource:[a-zA-Z0-9_-]+}/**";
        pattern = matcher.combine("/{api}", pattern);
        String path = "/api/ebay/order/1";

        assertTrue(matcher.isPattern(pattern));
        assertTrue(matcher.match(pattern, path));

        String internalPath = matcher.extractPathWithinPattern(pattern, path);

        String forwardedPath = "/other-api/order/";

        String translatedPath = forwardedPath + internalPath;

        assertEquals("/other-api/order/1", translatedPath);
    }

}
